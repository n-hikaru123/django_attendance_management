import sys
from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from attendance.models import Attendances
from .service.SampleDataInsertService import SampleDataInsertService
from .service.SampleDataDeleteService import SampleDataDeleteService
from .service.CSVfileService import CSVfileService
from .service.DetailCSVfileService import DetailCSVfileService


# データ設定画面を開く
class DataView(TemplateView):
    template_name = 'config_data/data.html'
    login_url = '/accounts/login/'

    def get(self, request):
        users = User.objects.values('id', 'username').order_by('id').all()
        print(users)
        data_count = []
        for user in users:
            user_info = {}
            # usersテーブルのidに紐づくattendancesテーブルのデータの件数を取得
            attendance_count = User.objects.get(pk=user['id']).attendances_set.all().count()
            user_info['id'] = user['id']
            user_info['name'] = user['username']
            user_info['attendance_count'] = attendance_count
            data_count.append(user_info)

        return self.render_to_response({'users':data_count})


# データ数の集計の場合
class CountDateView(TemplateView):

    def get(self, request):
        print(request)
        sample_data_insert_service = SampleDataInsertService()
        count = sample_data_insert_service.count_data()
        print(count)
        return JsonResponse({'count':count})


# サンプルデータをDBに登録する処理
class InitialDataInsertView(TemplateView):

    def get(self, request, *args, **kwargs):

        number_of_people = int(request.GET.get('number_of_people'))
        period = int(request.GET.get('period'))
        fix_request = int(request.GET.get('fix_request'))
        fix_request_accept = int(request.GET.get('fix_request_accept'))
        print(number_of_people)
        print(period)
        print(fix_request)
        print(fix_request_accept)

        # SampleDataInsertServiceクラスのインスタンスを作成
        sample_data_insert_service = SampleDataInsertService()
        user_ids = sample_data_insert_service.insert_users(number_of_people)

        if period == 0:
            return JsonResponse({'result':'新規ユーザーの作成'})
        all_days = sample_data_insert_service.insert_attendances(user_ids, period)

        if fix_request == 0:
            return JsonResponse({'result':'新規ユーザー、勤怠記録の作成'})
        sample_data_insert_service.insert_fix_request(user_ids, period, all_days)

        if fix_request_accept == 0:
            return JsonResponse({'result':'新規ユーザー、勤怠記録、修正申請の作成'})
        sample_data_insert_service.insert_fix_request_accept(user_ids)

        return JsonResponse({'result':'新規ユーザー、勤怠記録、修正申請、修正申請の諾否の作成'})


# サンプルデータをDBから削除する処理
class DeleteDataView(TemplateView):

    def get(self, request):
        sample_data_delete_service = SampleDataDeleteService()
        result = sample_data_delete_service.delete_data()
        data = {}
        if result == 'success':
            data['result'] = 'データを削除しました'
        else:
            data['result'] = 'データの削除に失敗しました'

        return JsonResponse(data)


# csvファイルを出力する処理
class CreateCSVView(TemplateView):

    def post(self, request):
        csv_file_service = CSVfileService()
        file_name = csv_file_service.create_csv()

        return JsonResponse({'result':file_name})


# 詳細情報記載のcsvファイルを出力する処理
class CreateDetailCSVView(TemplateView):

    def post(self, request):
        period = int(request.POST.get('get_period'))
        detail_csv_file_service = DetailCSVfileService()
        file_name = detail_csv_file_service.create_detail_csv(period)

        return JsonResponse({'result':file_name})
