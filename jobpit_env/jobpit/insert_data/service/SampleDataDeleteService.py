from django.contrib.auth.models import User
import sys
sys.path.append('../../')
from attendance.models import Attendances
from fix_request.models import AttendanceFixRequests
# from fix_request.views import FixAttendanceRequestView
from .SampleDataInsertService import SampleDataInsertService



# サンプルデータを削除する処理
class SampleDataDeleteService():

    def delete_data(self):
        # 各テーブルのデータを削除
        User.objects.all().delete()
        Attendances.objects.all().delete()
        AttendanceFixRequests.objects.all().delete()

        # 各テーブルのデータ数が０件であるか確認
        sample_data_insert_service = SampleDataInsertService()
        # count_tup:(User, Attendances, AttendanceFixRequests)
        count_tup = sample_data_insert_service.count_data()

        for count in count_tup:
            if not count == 0:
                return 'failure'
        return 'success'