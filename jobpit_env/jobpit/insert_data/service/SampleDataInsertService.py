import django
from django.db.models import query
from faker import Factory
from datetime import date, datetime, timedelta
from dateutil import relativedelta
import random
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password, make_password
import string
import sys
sys.path.append('../../')
from attendance.models import Attendances
from fix_request.models import AttendanceFixRequests
from fix_request.views import FixAttendanceRequestView
from itertools import chain

class SampleDataInsertService():

    # usersテーブルにサンプルデータ入れる処理
    def insert_users(self, number_of_people):

        # user_idが１のユーザーがいるかチェック
        is_user_id_1 = User.objects.filter(
            id = 1
        ).exists()
        if not is_user_id_1:
            start = 1
            end = number_of_people + 1
        else:
            start = User.objects.all().count() + 1
            end = start + number_of_people

        user_ids = []
        for count_id in range(start, end):

            user_ids.append(count_id)

            # ユーザー情報の登録
            fake = Factory.create('ja_JP')

            id = count_id
            # django標準のハッシュ化
            password = make_password('password')

            if not count_id == 1:
                is_user_id_1 = User.objects.filter(
                    id = 1
                ).exists()
            # user_idが１のユーザー名を「admin」にする
            while True:
                if not is_user_id_1:
                    username = 'admin'
                    first_name = ''
                    last_name = ''
                else:
                    name = fake.name()
                    arr_name = name.split(' ')
                    first_name = arr_name[0]
                    last_name = arr_name[1]
                    username = first_name + last_name
                # fakerで作成したusernameが被ることがある
                is_user = User.objects.filter(username=username).exists()
                if not is_user:
                    break

            start_day = datetime.strptime('1/1/2018 1:30 PM', '%m/%d/%Y %I:%M %p')
            end_day = datetime.strptime('12/31/2021 4:50 AM', '%m/%d/%Y %I:%M %p')
            last_login = fake.date_time_between(start_day, end_day)

            if username == 'admin':
                is_superuser = 1
            else:
                is_superuser = fake.random_int(0, 1)

            email = fake.email()

            if username == 'admin':
                is_staff = 1
            else:
                if is_superuser == 1:
                    is_staff = fake.random_int(0, 1)
                else:
                    is_staff = 0

            is_active = 1

            date_joined = fake.date_time_between(start_day, last_login)

            User.objects.create(
                id = id,
                password = password,
                last_login = last_login,
                is_superuser = is_superuser,
                username = username,
                first_name = first_name,
                last_name = last_name,
                email = email,
                is_staff = is_staff,
                is_active = is_active,
                date_joined = date_joined,
            )

        return user_ids


    # Attendancesテーブルにデータを入れる処理
    def insert_attendances(self, user_ids, period):

        all_days_flag = 1
        all_days = []
        # period:取得期間(何ヶ月)
        today = datetime.now()
        str_today = today.strftime('%Y-%m-%d')
        today = datetime.strptime(str_today, '%Y-%m-%d')
        three_months_ago = today - relativedelta.relativedelta(months=period)

        for user_id in user_ids:
            day = three_months_ago

            # ３ヶ月分に日時の配列
            # 出勤
            attendance_times = []
            # 退勤
            leave_times = []

            # 一周ごとに１日の出勤・退勤データを取得
            while True:
                if day < today:
                    # 3ヶ月分の年月日のすべてを取得
                    if all_days_flag == 1:
                        all_day = day
                        srt_all_day = all_day.strftime('%Y-%m-%d')
                        all_days.append(srt_all_day)

                    # 5日間に2日の確率で出勤ボタンを押下していない
                    not_attendance_time = random.randint(1,5)
                    if not_attendance_time == 1 or not_attendance_time == 2:
                        day = day + timedelta(days=1)
                        continue

                    # 作成した年月日時分秒のデータを配列attendance_timeに入れる
                    day = day.replace(hour=random.randint(8,10), minute=random.randint(0,59), second=random.randint(0,59), microsecond=0)
                    srt_day = day.strftime('%Y-%m-%d %H:%M:%S')
                    attendance_times.append(srt_day)

                    # 作成した年月日時分秒のデータを配列attendance_timeに入れるleave_time
                    # 出勤データがある場合でも5分の2の確率で退勤データが存在しない
                    not_leave_time = random.randint(1,5)
                    # 出勤データが存在する場合で退勤データを入れない時、退勤データにNoneを入れる
                    if not_leave_time == 1 or not_leave_time == 2:
                        leave_times.append(None)
                        day = day + timedelta(days=1)
                        continue

                    day = day.replace(hour=random.randint(17,23), minute=random.randint(0,59), second=random.randint(0,59), microsecond=0)
                    srt_day = day.strftime('%Y-%m-%d %H:%M:%S')
                    leave_times.append(srt_day)

                    day = day + timedelta(days=1)
                else:
                    break

            all_days_flag = 0

            # Attendancesテーブルにデータを挿入
            # attendance_timesとleave_timesの要素数は同じ
            for i in range(len(attendance_times)):
                Attendances.objects.create(
                    attendance_time = attendance_times[i],
                    leave_time = leave_times[i],
                    user_id = user_id,
                )

        return all_days


    # AttendancesFixRequestsテーブルにデータを入れる処理
    def insert_fix_request(self, user_ids, period, all_days):

        # 1人につき30日間で6日間分の確率で打刻修正の申請を行う
        # 修正申請を行うベースとなる数 = 30 * ユーザー数 * 何ヶ月 // 5
        fix_request_count = 30 * len(user_ids) * period // 5
        print(fix_request_count)

        # 修正申請を行うユーザー・日にちの組合せをランダムで取得
        access_records = []
        while True:
            access_record = []
            user_id = random.choice(user_ids)
            date = random.choice(all_days)
            access_record.extend([user_id, date])

            # access_recordsに同一のユーザー・年月日がない場合、追加する
            if not access_record in access_records:
                access_records.append(access_record)
            if len(access_records) == fix_request_count:
                break

        # 申請修正申請
        arr_push_type = ['AT', 'LE']

        for access_record in access_records:
            # 日付
            push_date = access_record[1]
            # 打刻種類
            push_type = random.choice(arr_push_type)
            # 修正時刻
            push_time = self.get_request_time(push_type)
            # 修正理由
            push_reason = self.get_reason()
            # 対象ユーザー
            user = access_record[0]

            request_info = {
                'push_date' : push_date,
                'push_type' : push_type,
                'push_time' : push_time,
                'push_reason' : push_reason,
                'user' : user,
            }

            # 同一ユーザーが同一日に出勤と退勤の修正申請を行う処理
            loop_flg = 0
            while True:
                if loop_flg == 0:
                    self.db_insert_fix_request(request_info)
                    loop_flg += 1
                elif loop_flg == 1:
                    i = random.randint(1,5)
                    if i == 1:
                        if request_info['push_type'] == 'AT':
                            request_info['push_type'] = 'LE'
                        elif request_info['push_type'] == 'LE':
                            request_info['push_type'] = 'AT'
                        request_info['push_time'] = self.get_request_time(request_info['push_type'])
                        request_info['push_reason'] = self.get_reason()
                        self.db_insert_fix_request(request_info)
                        loop_flg += 1
                    else:
                        loop_flg += 1
                else:
                    break


    # リクエスト時間を取得
    def get_request_time(self, push_type):
        if push_type == 'AT':
            start_time = datetime.strptime('8:00 AM', '%I:%M %p')
            end_time = datetime.strptime('10:59 AM', '%I:%M %p')
        elif push_type == 'LE':
            start_time = datetime.strptime('5:00 PM', '%I:%M %p')
            end_time = datetime.strptime('11:59 PM', '%I:%M %p')
        fake = Factory.create('ja_JP')
        random_time = fake.date_time_between(start_time, end_time)
        push_time = random_time.strftime("%H:%M")
        return push_time


    # 修正理由を取得
    def get_reason(self):
        hiragana = [chr(i) for i in range(ord("ぁ"), ord("ん")+1)]
        hiragana = "".join(hiragana)
        length_of_string = random.randint(8,20)
        push_reason = ''.join(random.SystemRandom().choice(hiragana) for _ in range(length_of_string))
        return push_reason


    # 修正申請のデータを登録する処理
    def db_insert_fix_request(self, request_info):
        print(request_info)
        FixAttendanceRequestView.post(
            'dummy', 'dummy', request_key=request_info
        )


    # 承認・拒絶を行う処理
    def insert_fix_request_accept(self, user_ids):
        # 更新するレコードを取得
        # AttendanceFixRequestsの空のquerySet
        merge_query_set = AttendanceFixRequests.objects.none()
        for user_id in user_ids:
            is_fix_request = AttendanceFixRequests.objects.filter(user_id=user_id).exists()
            if is_fix_request:
                fix_request = AttendanceFixRequests.objects.filter(user_id=user_id)
                # ループで取得したquerySetをmerge_query_setにマージ
                merge_query_set = list(chain(merge_query_set, fix_request))

        # 承認・拒絶を行うDBに登録する処理
        for fix_request in merge_query_set:
            is_accept_random = random.randint(1,3)

            # 1：承認
            if is_accept_random == 1:
                fix_request.is_accepted = True
                fix_request.checked_time = datetime.now()
            # 2:却下
            elif is_accept_random == 2:
                fix_request.checked_time = datetime.now()
            # 3:未確認

            fix_request.save()


    # 各テーブルのデータ数を取得
    def count_data(self):
        count_user = User.objects.all().count()
        count_attendance = Attendances.objects.all().count()
        count_attendance_fix_request = AttendanceFixRequests.objects.all().count()
        # 複数をreturnする場合、タプルになる
        return count_user, count_attendance, count_attendance_fix_request
