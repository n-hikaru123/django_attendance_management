import os
import getpass
from datetime import datetime, date, time, timedelta
from dateutil.relativedelta import relativedelta
import sys
# from matplotlib.cbook import pts_to_midstep
from injector import inject
from django.contrib.auth.models import User
sys.path.append('../../')
from attendance.models import Attendances
from fix_request.models import AttendanceFixRequests
from .CSVfileNameWriteService import CSVfileNameWriteService

class DetailCSVfileService:

    # 依存性の注入
    @inject
    def __init__(self):
        self.file_name_write = CSVfileNameWriteService

    def create_detail_csv(self, period):

        # 出力するCSVファイル名を取得
        name = '_attendances_detail'
        result = self.file_name_write.csv_file_name(name)
        output_name = result[0]
        output_path = result[1]

        # 取得期間の初日を取得
        now = datetime.now()
        start_date = now - relativedelta(months=period)
        start_date = start_date.strftime('%Y-%m-%d')

        # https://codelab.website/django-queryset-filter/#toc12
        # attendance_time__date <= start_date
        # 出勤の年月日　の古い順番
        attendances = Attendances.objects.order_by('attendance_time').filter(
            attendance_time__date__gte=start_date
        ).all()

        attendances_data = []

        for attendance in attendances:

            # 氏名
            username = User.objects.values_list('username', flat=True).filter(
                pk=attendance.user_id
            ).get()
            # 出勤時間
            attendance_time = attendance.attendance_time

            # 退勤時間, 労働時間
            leave_time = attendance.leave_time
            if leave_time:
                time = str(leave_time - attendance_time)
                # 退勤時間, 労働時間 CSVファイル表示用
                leave_time = leave_time.strftime('%Y-%m-%d %H:%M:%S')
            else:
                leave_time = ''
                time = ''

            # 出勤時間 CSVファイル表示用
            attendance_time = attendance_time.strftime('%Y-%m-%d %H:%M:%S')

            # 修正申請の有無を確認
            is_fix_request = AttendanceFixRequests.objects.filter(
                attendance_id=attendance.id
            ).exists()

            # 修正申請がある場合
            if is_fix_request:
                fix_requests = AttendanceFixRequests.objects.filter(
                    attendance_id=attendance.id
                ).all()

                # 修正申請をCSVファイル出力用に成形
                fix_data = self.fix_request_date(fix_requests)

            # 修正申請がない場合
            else:
                fix_requests = None
                fix_data = self.fix_request_date(fix_requests)

            # 取得データをレコードにまとめる
            row = []
            row.extend([username, attendance_time, leave_time, time])
            for data in fix_data.values():
                row.append(data)

            # CSVファイル作成に必要なデータをまとめる
            attendances_data.append(row)


        column_name = '氏名, 出勤時間, 退勤時間, 勤務時間, '\
        '修正申請時刻(出勤), 修正申請のリクエストを行った時刻(出勤), 申請の状況(出勤), 削除の有無(出勤)' \
        '修正申請時刻(退勤), 修正申請のリクエストを行った時刻(退勤), 申請の状況(退勤), 削除の有無(退勤)'

        # ファイルに書き込む
        result = self.file_name_write.csv_file_write(output_path, output_name, attendances_data, column_name)

        return output_name


    # 修正申請をCSVファイル出力用に成形
    def fix_request_date(self, fix_requests):

        # 初期値
        data = {
            'revision_time_attendance': '',
            'request_time_attendance': '',
            'request_attendance_situation': '',
            'request_attendance_deleted': '',
            'revision_time_leave': '',
            'request_time_leave': '',
            'request_leave_situation': '',
            'request_leave_deleted': ''
        }

        # 出勤と退勤の両方の修正申請がない場合、初期値を返す
        if fix_requests == None:
            return data

        # fix_requestsに①出勤の修正申請情報のみ、②退勤の修正申請情報のみ、又は、③出勤退勤両方の修正申請があることがあるためループで回す
        for fix_request in fix_requests:
            # 出勤の場合
            if fix_request.stamp_type == 'AT':
                # 出勤の修正申請時刻
                data['revision_time_attendance'] = fix_request.revision_time.strftime('%Y-%m-%d %H:%M:%S')
                # 出勤の修正申請のリクエストを行った時刻
                data['request_time_attendance'] = fix_request.request_time.strftime('%Y-%m-%d %H:%M:%S')
                # 削除の有無
                if not fix_request.deleted_at == None:
                    data['request_attendance_deleted'] = fix_request.deleted_at.strftime('%Y-%m-%d %H:%M:%S')
                # 申請の状況
                if fix_request.checked_time and fix_request.is_accepted == 1:
                    data['request_attendance_situation'] = 'accepted'
                elif fix_request.checked_time and fix_request.is_accepted == 0:
                    data['request_attendance_situation'] = 'rejected'
                else:
                    data['request_attendance_situation'] = 'not_checked'
            # 退勤の場合
            else:
                # 退勤の修正申請時刻
                data['revision_time_leave'] = fix_request.revision_time.strftime('%Y-%m-%d %H:%M:%S')
                # 退勤の修正申請のリクエストを行った時刻
                data['request_time_leave'] = fix_request.request_time.strftime('%Y-%m-%d %H:%M:%S')
                # 削除の有無
                if not fix_request.deleted_at == None:
                    data['request_leave_deleted'] = fix_request.deleted_at.strftime('%Y-%m-%d %H:%M:%S')
                # 申請の状況
                if fix_request.checked_time and fix_request.is_accepted == 1:
                    data['request_leave_situation'] = 'accepted'
                elif fix_request.checked_time and fix_request.is_accepted == 0:
                    data['request_leave_situation'] = 'rejected'
                else:
                    data['request_leave_situation'] = 'not_checked'

        return data
