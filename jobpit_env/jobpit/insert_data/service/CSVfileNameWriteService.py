import os
import getpass
from datetime import datetime, date, time, timedelta

class CSVfileNameWriteService():

    # ファイル名を取得
    def csv_file_name(name):

        today = date.today()
        # OSにログイン中のユーザーID(ログイン名)を取得
        user = getpass.getuser()
        output_path = '/Users/{user}/Downloads/'.format(user=user)
        file_name = today.strftime('%Y%m%d') + name
        output_name = ''

        # 当日日付の対象ファイルが存在するか確認
        is_file = os.path.exists('{output_path}{file_name}.csv'.format(output_path=output_path, file_name=file_name))
        # 対象ファイルが存在する場合は、ファイル名に番号を付ける
        if is_file:
            file_count = 1
            while True:
                file_check_name = file_name + '(' + str(file_count) + ')'
                output_name= '{file_check_name}.csv'.format(file_check_name=file_check_name)
                is_file = os.path.exists('{output_path}{output_name}'.format(output_path=output_path, output_name=output_name))
                if not is_file:
                    break
                file_count += 1
        else:
            output_name = file_name + '.csv'

        return output_name, output_path


    # ファイルに書き込む
    def csv_file_write(output_path, output_name, attendances_data, column_name):

        with open(output_path + output_name, 'w', encoding='UTF-8') as csv_file:
            # csv_file.write('ユーザーID, ユーザー名, 出勤時間, 退勤時間, 勤務時間\n')
            csv_file.write(column_name + '\n')
            for row in attendances_data:
                count = 1
                els = len(row)
                for el in row:
                    csv_file.write(el)
                    if not els == count:
                        csv_file.write(', ')
                    else:
                        csv_file.write(',')
                    count += 1
                csv_file.write('\n')

