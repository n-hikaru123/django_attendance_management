from email import header
# from datetime import datetime, date, time, timedelta
from injector import inject
from django.contrib.auth.models import User
import sys
sys.path.append('../../')
from attendance.models import Attendances
from .CSVfileNameWriteService import CSVfileNameWriteService

class CSVfileService():

    @inject
    def __init__(self):
        self.file_name_write = CSVfileNameWriteService

    def create_csv(self):

        # 出力するCSVファイル名を取得
        name = '_attendances'
        result = self.file_name_write.csv_file_name(name)
        output_name = result[0]
        output_path = result[1]

        # csvファイルに出力するデータを生成
        is_user = User.objects.filter().exists()
        if is_user:
            users = User.objects.values('id', 'username').order_by('id').all()

        attendances_data = []
        for user in users:
            attendances = User.objects.get(pk=user['id']).attendances_set.all()
            for attendance in attendances:
                # ユーザーID
                user_id = str(user['id'])
                # ユーザー名
                user_name = user['username']
                # 出勤時間
                attendance_time = attendance.attendance_time.strftime('%Y-%m-%d %H:%M:%S')
                if not attendance.leave_time == None:
                    # 退勤時間
                    leave_time = attendance.leave_time.strftime('%Y-%m-%d %H:%M:%S')
                    # 勤務時間
                    time = str(attendance.leave_time - attendance.attendance_time)
                else:
                    # 退勤時間
                    leave_time = ''
                    # 勤務時間
                    time = ''

                row = []
                row.extend([user_id, user_name, attendance_time, leave_time, time])

                attendances_data.append(row)


        column_name = 'ユーザーID, ユーザー名, 出勤時間, 退勤時間, 勤務時間'
        # ファイルに書き込む
        result = self.file_name_write.csv_file_write(output_path, output_name, attendances_data, column_name)

        return output_name
