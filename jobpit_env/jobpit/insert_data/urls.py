from django.urls import path
from.views import (
    DataView,
    CountDateView,
    InitialDataInsertView,
    DeleteDataView,
    CreateCSVView,
    CreateDetailCSVView
)

# as_view:viewオブジェクトを生成
urlpatterns = [
    # data
    # データ設定画面を開く
    path('', DataView.as_view(), name='data'),
    # data/count-data
    # 各サンプルデータ数を取得
    path('count-data', CountDateView.as_view(), name='count-data'),
    # data/initial-data
    # 各サンプルデータを作成
    path('initial-data', InitialDataInsertView.as_view(), name='initial-data'),
    # data/delete-data
    # 各サンプルデータを削除
    path('delete-data', DeleteDataView.as_view(), name='delete-data'),
    # data/create-csv
    # csvファイルを出力
    path('create-csv', CreateCSVView.as_view(), name='create-csv'),
    # data/create-csv
    # 詳細情報記載のcsvファイルを出力
    path('create-csv-detail', CreateDetailCSVView.as_view(), name='create-csv-detail')
]