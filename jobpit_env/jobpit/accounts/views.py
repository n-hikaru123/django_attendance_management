# djangoの便利機能
from django.contrib.auth.forms import UserCreationForm
# URLの指定を行う時に使う
from django.urls import reverse_lazy
# djangoの汎用view（処理）
from django.views import generic

class SignUpView(generic.CreateView):
    # CreateViewはgeneric（汎用view）の一機能

    form_class = UserCreationForm
    # success_urlでは登録完了後のリダイレクト先のURLを指定
    # jobpit_env/lib/python3.8/site-packages/django/contrib/admin/sites.py
    # path('login/', self.login, name='login')
    success_url = reverse_lazy('login')
    # template_nameでは登録画面のHTMLファイル名を指定
    template_name = 'registration/signup.html'