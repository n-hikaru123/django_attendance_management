from django.test import TestCase, Client
from django.contrib.auth.models import User

class LoginRedirectTest(TestCase):
    # setUpメソッド:各テストを実行する前に共通して行う処理を記述
    def setUp(self):
        # テスト用のユーザーを作成
        self.credentials = {
            'username' : 'testuser',
            'password' : 'samplesecret'
        }
        # User.objects.create_user():ユーザーを作成
        # create_user()メソッドはパスワード引数をハッシュする
        # **：self.credentialsのキーワード引数が複数入る
        User.objects.create_user(**self.credentials)
        # この時点でこのUserオブジェクトはsaveされている

        # テストクライアントのインスタンスを設定
        # unittestを使う場合、毎回Clientを生成する必要がある。
        self.client = Client()

    def test_redirect(self):
        # メソッドブロックの頭に'''でコメントを囲むと、テストに失敗したときにコメントの内容が表示される
        '''
        ログインしていない状態の時、リダイレクトされたか確認するテスト
        '''
        # リダイレクト情報を取得
        # GETメソッドでのアクセス
        # 第１引数：URL
        # follow=Trueとすることで、クライアントはリダイレクト先まで辿っていく。
        # https://runebook.dev/ja/docs/django/topics/testing/tools
        response = self.client.get('/', follow=True)
        # リダイレクト先のURLを取得
        redirect_url = response.redirect_chain[0][0]
        self.assertEqual(redirect_url, '/accounts/login/?next=/')

    def test_not_redirect(self):
        '''
        ログイン状態の時、リダイレクトされないか確認するテスト
        '''
        # テストユーザーでログイン
        self.client.login(
            username = self.credentials['username'],
            password = self.credentials['password']
        )
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
