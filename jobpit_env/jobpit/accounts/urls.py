from django.urls import path
# accountsディレクトリ内にあるviews.pyのSignUpViewクラスを呼び出しています。
from .views import SignUpView


urlpatterns = [
    # Viewクラスで処理を行う。（laravelのコントローラーのようなもの）
    # URLにsignup/を指定して、SignUpViewクラスを呼び出すようにします。
    # この処理は「accounts/signup/」が指定された時に呼び出される　→　プロジェクト管理フォルダのurls.pyで「accounts/」の指定あり
    path('signup/', SignUpView.as_view(), name='signup')
]