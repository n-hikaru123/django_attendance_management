import sys
from django.http.response import JsonResponse
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from .models import Attendances
from datetime import date, datetime

import requests

from . import scraping_views
from . import weatherAPI_views

import environ
# envファイル
env = environ.Env()
# .envを読み込む
environ.Env.read_env('.env')

# 出勤・退勤ボタンの画面に遷移する時の処理
# LoginRequiredMixin viewsでログインユーザのみの実行を許可をする（クラスベース）
    # ログインしている場合→呼ばれたクラスの関数を実行する。
    # ログインしていない場合→LOGIN_URLで指定したURL先へリダイレクトする。
# TemplateView 汎用viewsの使用：依存性の注入を行い、使用できるようにする
class HomeView(LoginRequiredMixin, TemplateView):
    # 表示するテンプレートを指定
    template_name = 'home.html'
    # ログインがされてなかったらリダイレクトされるURL
    login_url = '/accounts/login/'

    def get(self, request):

        # 天候情報などをwebスクレイピングで取得する場合
        # テンプレート側ではparts/scraping_weather.htmlをincludeする
        # {% include 'parts/scraping_weather.html' %}
        # weather_forecast = scraping_views.get_weather_forecast(self)

        weather_forecast = weatherAPI_views.get_apidata(self)
        return self.render_to_response(weather_forecast)


# 出勤・退勤ボタンを押下した時の処理
# Ajax
class PushTimecard(LoginRequiredMixin, TemplateView):
    # ログインがされてなかったらリダイレクトされるURL
    login_url = '/accounts/login/'

    # POSTメソッドでリクエストされたら実行するメソッド
    # home.htmlのajaxから呼ばれる
    def post(self, request, *args, **kwargs):
        print('requestログ', request.user)
        # get()メソッドでは第一引数にキーを指定し、第二引数にそのキーが存在しなかった場合に返す値を指定
        push_type = request.POST.get('push_type')
        print('push_typeログ', push_type)

        # filter関数で取得するオブジェクトの条件を指定
        # exists()を追加すると条件当てはまるデータが存在するかどうかを返す
        is_attendanced = Attendances.objects.filter(
            # requestしたuser
            user = request.user,
            # 現在年月日
            attendance_time__date = date.today()
        ).exists()
        print('is_attendancedログ', is_attendanced)

        is_left = Attendances.objects.filter(
            user = request.user,
            leave_time__date = date.today()
        ).exists()

        response_body = {}
        # 出勤の場合
        if push_type == 'attendance' and not is_attendanced:
            # 出勤したユーザーをDBに保存する
            # 現在ログインしているユーザーのインスタンスを作成
            attendance = Attendances(user=request.user)
            attendance.attendance_time = self.getNowTime()
            attendance.save()

            response_body = {
                'result': 'success',
                'attendance_time': attendance.attendance_time.strftime('%H:%M:%S')
            }

        # 退勤の場合
        elif push_type == 'leave' and not is_left:
            if is_attendanced:
                # 退勤するユーザーのレコードの退勤時間を更新する
                # filterの条件に該当するレコードの1番目を取得(複数出勤が登録された時のため)
                attendance = Attendances.objects.filter(
                    user = request.user,
                    attendance_time__date = date.today()
                )[0]
                attendance.leave_time = self.getNowTime()
                attendance.save()

                response_body = {
                    'result': 'success',
                    'leave_time': attendance.leave_time.strftime('%H:%M:%S')
                }
            # 本日の出勤の記録はないが、退勤ボタンが押下された時
            else:
                response_body = {
                    'result': 'not_attended',
                }
        # 出勤データがあるが出勤が押下された場合、又は、退勤データがあるが退勤が押下された場合
        if not response_body:
            print('if not response_body:', response_body)
            response_body = {
                'result': 'already_exists'
            }

        # Line通知
        self.line_notice(request.user, response_body)

        # Ajaxからの呼び出しで値を返す
        return JsonResponse(response_body)


    # 現在年月日時分秒を取得
    def getNowTime(self):
        #isoformat(timespec='seconds')で秒より下を0で丸める。strに変換される
        str_response_time = datetime.now().isoformat(timespec='seconds')
        str_response_time = str_response_time.replace('T', ' ')
        # datetime型に変換
        response_time = datetime.strptime(str_response_time, '%Y-%m-%d %H:%M:%S')
        return response_time

    # Line通知
    def line_notice(self, user, response_body):
        if 'attendance_time' in response_body:
            type = '出勤'
            contents = [str(user), type, response_body['result']]
        elif 'leave_time' in response_body:
            type = '退勤'
            contents = [str(user), type, response_body['result']]
        else:
            contents = [str(user), response_body['result']]

        ACCESS_TOKEN = env('LINE_ACCESS_TOKEN')

        headers = {"Authorization": f"Bearer {ACCESS_TOKEN}"}

        data = {
            "message": '\r\n'.join(contents)
        }

        requests.post(
            "https://notify-api.line.me/api/notify",
            headers=headers,
            data=data,
        )


# １ヶ月ごとの勤務表を表示する処理
class AttendanceRecords(LoginRequiredMixin, TemplateView):
    # 共有するものを書く
    login_url = '/accounts/login/'
    template_name = 'attend_records.html'

    def get(self, request, *args, **kwarge):
        print('AttendanceRecords.get送信')
        today= datetime.today()
        # リクエストパラメータ(キー：year_month)を受け取る
        search_param = request.GET.get('year_month')
        if search_param:
            search_params = list(map(int, search_param.split('-')))
            search_year = search_params[0]
            search_month = search_params[1]
        # 年月の絞り込みがない場合
        else:
            search_year = today.year
            search_month = today.month
        # 年と月でデータを絞り込む
        month_attendances = Attendances.objects.filter(
            user = request.user,
            attendance_time__year = search_year,
            attendance_time__month = search_month,
        ).order_by('attendance_time')

        for data in month_attendances:
            print(data)
            print(data.user)
            print(data.attendance_time)
            print(data.leave_time)

        # 取得したデータを整形
        attendances_context = []
        for attendance in month_attendances:
            # オブジェクト.要素で値を取得
            attendance_time = attendance.attendance_time
            # print('today.date()', today.date())
            leave_time = attendance.leave_time
            if attendance_time and leave_time:
                time = leave_time - attendance_time
                time = str(time)
                # 時間を２桁にする
                arr_time = time.split(':')
                if int(arr_time[0]) < 10:
                    arr_time[0] = '0' + arr_time[0]
                    time = ':'.join(arr_time)
            # 勤怠時間のデータがない場合
            else:
                time = None

            if leave_time:
                leave_time = leave_time.strftime('%H:%M:%S')
            # 退勤のデータがない場合
            else:
                # 本日の場合
                if attendance_time.date() == today.date():
                    leave_time = None
                else:
                    leave_time = 'not_pushed'

            day_attendance = {
                'date':attendance_time.strftime('%Y-%m-%d'),
                'attendance_at':attendance_time.strftime('%H:%M:%S'),
                'leave_at':leave_time,
                'time':time
            }
            # テンプレートに渡す配列に要素を追加
            attendances_context.append(day_attendance)

        # テンプレートでは「attendances」をキーに「attendances_context」のデータを取得
        context = {'attendances':attendances_context}

        # Templateにcontextを含めてレスポンスを返す
        return self.render_to_response(context)
