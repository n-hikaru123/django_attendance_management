from django.db import models
# Djangoに標準で備わっているUserモデル
# jobpit_env/lib/python3.8/site-packages/django/contrib/auth/models.py(Auth_userテーブル)
from django.contrib.auth.models import User
from datetime import datetime

# import datetime

class Attendances(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    attendance_time = models.DateTimeField(default=datetime.now)
    # attendance_time = models.DateTimeField(default=datetime.date.today() - datetime.timedelta(days=1))
    leave_time = models.DateTimeField(null=True)


# 1.idはテーブルのプライマリーキーになるのでModel上では自動で設定されるので記述はいりません。
# 2.user_idはAttendancesモデル上の変数では、userとしています。
#   これはUserモデルのデータを呼び出すためにmodels.ForeignKeyを使うことで、Userモデルのプライマリーキーであるidを参照しています。
# 3.on_delete=models.CASCADEは参照しているオブジェクトが削除されたら該当のデータも一緒に削除
# 4.default=datetime.nowを使えばデータを保存するときに現在時刻でattendance_timeの値を設定してくれます。