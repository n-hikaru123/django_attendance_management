# 正規表現モジュール
import re
# スクレイピング
import requests
from bs4 import BeautifulSoup

# 東京の天気予報を取得
def get_weather_forecast(self):
    load_url = 'https://weather.yahoo.co.jp/weather/jp/13/4410.html'
    html = requests.get(load_url)
    soup = BeautifulSoup(html.content, "html.parser")

    # 今日明日の天気
    weathers = soup.find_all('p', class_='pict')
    today_tomorrow_weather = []
    for weather in weathers:
        weather = weather.get_text()
        # \nとスペースを一括削除
        weather = re.sub(r"[\n ]", "", weather)
        today_tomorrow_weather.append(weather)

    # 最高温度・最低温度
    temps = soup.find_all('ul', class_='temp')
    today_tomorrow_temp = []
    for temp in temps:
        temp = temp.get_text()
        temp = temp[1:-1]
        today_tomorrow_temp.append(temp)

    # 洗濯、傘、紫外線、重ね着、乾燥、風邪注意
    informations = soup.find_all('p', class_='index_text')
    today_information = []
    tomorrow_information = []
    i = 0
    cass = len(informations) // 2
    # informationsの前6個が今日の情報、後ろ6個が明日の情報
    for information in informations:
        information = information.get_text()
        if i < cass:
            today_information.append(information)
        else:
            tomorrow_information.append(information)
        i += 1

    context = {
        'today_weather': today_tomorrow_weather[0],
        'today_temp': today_tomorrow_temp[0],
        'today_Washing': today_information[0],
        'today_umbrella': today_information[1],
        'today_uv': today_information[2],
        'today_clothes': today_information[3],
        'today_dry': today_information[4],
        # 'today_cold': today_information[5], WEBサイトにデータがない
        'tomorrow_weather': today_tomorrow_weather[1],
        'tomorrow_temp': today_tomorrow_temp[1],
        'tomorrow_Washing': tomorrow_information[0],
        'tomorrow_umbrella': tomorrow_information[1],
        'tomorrow_uv': tomorrow_information[2],
        'tomorrow_clothes': tomorrow_information[3],
        'tomorrow_dry': tomorrow_information[4],
        # 'tomorrow_cold': tomorrow_information[5],　WEBサイトにデータがない
    }

    return context