from django.urls import path
from .views import HomeView, PushTimecard, AttendanceRecords

urlpatterns = [
    # トップ画面（出勤簿）
    path('', HomeView.as_view(), name='home'),
    # /push
    path('push', PushTimecard.as_view(), name='push'),
    # /records
    path('records', AttendanceRecords.as_view(), name='records')
]