from turtle import color
import requests
import re
from datetime import datetime
import matplotlib
matplotlib.use('Agg') # Matplotlibクラッシュ回避
import matplotlib.pyplot as plt
import numpy as np
import base64
import io
from io import BytesIO

import sys
import os
# 2つ上のディレクトリに属するモジュールを使う
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from consts import WEATHER_CODE

# APIデータ
json_data = {}
# 取得するデータのインデックス
index_list = []
# 対象時間
time_list = []
# 現在から24時間分の気温
temperature_from_now_list = []
# 本日と翌日の最高気温
temperature_max_list = []
# 本日と翌日の最低気温
temperature_min_list = []
# 本日24時間分の気温
get_temperature_today24_list = []
# 湿度
relativehumidity_from_now_list = []
# 日の出の時間
sunrise_list = []
# 日没の時間
sunset_list = []
# 現在の気象条件
current_weather = {}

# 全ての時間を取得
all_time_list = []
# 全ての気温を取得
all_temperature_list = []
# 全ての湿度を取得
all_relativehumidity_list = []

# 本日の0時から翌日の0時までの25個
x_24h = [i for i in range(0, 24+1)]
# 3時間ごとの時間
every_3_h = [0, 3, 6, 9, 12, 15, 18, 21, 24]
# 現在分を少数に変換
dt_now = datetime.now()
time_a_few_num = int(dt_now.hour) + int(dt_now.minute) * (1/60)


def get_apidata(self):
    # APIデータ取得
    get_json_data()
    # 対象時間の取得
    get_time()
    # 気温を取得
    get_temperature_from_now()
    # 最高気温を取得
    get_temperature_max()
    # 最低気温を取得
    get_temperature_min()
    # 本日24時間分の気温を取得
    get_temperature_today24()
    # 湿度を取得
    get_relativehumidity_from_now()
    # 本日24時間分の湿度を取得
    get_relativehumidity_today24()
    # 日の出の時間を取得
    get_sunrise()
    # 日没の時間を取得
    get_sunset()
    # 現在の気象条件を取得
    get_current_weather()

    # 本日の気温・湿度それぞれの折線グラフを作成
    temperature_grelativehumidity_graph = create_temperature_relativehumidity_plot()
    # 本日の気温・湿度の折線グラフを作成
    temperature_grelativehumidity_mix_graph = create_temperature_relativehumidity_mix_plot()
    # 一週間分の気温・湿度の折線グラフを作成
    temperature_grelativehumidity_mix_week_graph = create_temperature_relativehumidity_mix_week_plot()

    context = {
        'time_list': time_list,
        'temperature_from_now_list': temperature_from_now_list,
        'temperature_max_list': temperature_max_list,
        'temperature_min_list': temperature_min_list,
        'temperature_today24_list': temperature_today24_list,
        'relativehumidity_from_now_list': relativehumidity_from_now_list,
        'relativehumidity_today24_list': relativehumidity_today24_list,
        'sunrise_list': sunrise_list,
        'sunset_list': sunset_list,
        'current_weather': current_weather,
        'temperature_grelativehumidity_graph': temperature_grelativehumidity_graph,
        'temperature_grelativehumidity_mix_graph':temperature_grelativehumidity_mix_graph,
        'temperature_grelativehumidity_mix_week_graph':temperature_grelativehumidity_mix_week_graph
    }

    return context

def get_json_data():
    # 緯度 latitude(東京)
    # 軽度 ongitude(東京)
    # 気温(1時間ごと):'temperature_2m',
    # 最高気温 temperature_2m_max,
    # 最低気温 temperature_2m_min,
    # 湿度　relativehumidity_2m
    # 日の出 sunrise,
    # 日没 sunset,
    # タイムゾーン東京　Asia%2FTokyo,
    # 現在の気象条件 current_weather
        # ex
        # {'temperature': 33.7, 'windspeed': 16.5, 'winddirection': 191.0, 'weathercode': 3.0, 'time': '2022-07-02T15:00'}
    url = 'https://api.open-meteo.com/v1/forecast?latitude=35.6785&longitude=139.6823&hourly=temperature_2m,relativehumidity_2m&daily=temperature_2m_max,temperature_2m_min,sunrise,sunset&current_weather=true&timezone=Asia%2FTokyo'
    res = requests.get(url)
    global json_data
    json_data = res.json()


def get_time():
    global time_list
    time_list = []
    time_list_str = json_data['hourly']['time']

    # 取得した文字列の時間のデータをdatetime型に変換(現在時刻と比較するため)
    time_list_dt = []
    for time in time_list_str:
        time_info = re.split('[-T:]',time)
        for i in range(len(time_info)):
            time_info[i] = int(time_info[i])
        time_dt = datetime(time_info[0], time_info[1], time_info[2], time_info[3], time_info[4])
        time_list_dt.append(time_dt)

    # 上で取得したdatetime型の時間のリストと現在時刻を比較　現在時刻より12時間分の後の時間を取得
    global index_list
    index_list = []
    dt_now = datetime.now()
    for i,time in enumerate(time_list_dt):
        if dt_now < time:
            index_list.append(i)
            time_h = time.strftime('%H')
            # 先頭の"0"を削除(数値に変換)
            time_h = int(time_h)
            time_list.append(time_h)
            # 本日の0時から翌日の0時までの25個の気温を表示
            if len(index_list) == 25:
                break

    global all_time_list
    all_time_list = []
    for time in time_list_dt:
        time_h = time.strftime('%H')
        # 先頭の"0"を削除(数値に変換)
        time_h = int(time_h)
        all_time_list.append(time_h)


def get_temperature_from_now():
    # 対象時間内の気温を取得
    global temperature_from_now_list
    temperature_from_now_list = []
    for index in index_list:
        temperature_from_now_list.append(json_data['hourly']['temperature_2m'][index])

    # 全ての気温を取得
    global all_temperature_list
    all_temperature_list = json_data['hourly']['temperature_2m']


def get_temperature_max():
    global temperature_max_list
    temperature_max_list = []
    temperature_max_list.append(json_data['daily']['temperature_2m_max'][0])
    temperature_max_list.append(json_data['daily']['temperature_2m_max'][1])


def get_temperature_min():
    global temperature_min_list
    temperature_min_list = []
    temperature_min_list.append(json_data['daily']['temperature_2m_min'][0])
    temperature_min_list.append(json_data['daily']['temperature_2m_min'][1])


def get_temperature_today24():
    global temperature_today24_list
    temperature_today24_list = []
    for i in range(24+1):
        temperature_today24_list.append(json_data['hourly']['temperature_2m'][i])


def get_relativehumidity_from_now():
    global relativehumidity_from_now_list
    relativehumidity_from_now_list = []
    for index in index_list:
        relativehumidity_from_now_list.append(json_data['hourly']['relativehumidity_2m'][index])

    # 全ての湿度を取得
    global all_relativehumidity_list
    all_relativehumidity_list = json_data['hourly']['relativehumidity_2m']

def get_relativehumidity_today24():
    global relativehumidity_today24_list
    relativehumidity_today24_list = []
    for i in range(24+1):
        relativehumidity_today24_list.append(json_data['hourly']['relativehumidity_2m'][i])


def get_sunrise():
    global sunrise_list
    sunrise_list = []
    sunrise_list.append(json_data['daily']['sunrise'][0])
    sunrise_list.append(json_data['daily']['sunrise'][1])


def get_sunset():
    global sunset_list
    sunset_list = []
    sunset_list.append(json_data['daily']['sunset'][0])
    sunset_list.append(json_data['daily']['sunset'][1])


def get_current_weather():
    global current_weather
    current_weather = json_data['current_weather']
    for code_set in WEATHER_CODE:
        if code_set[0] == current_weather['weathercode']:
            current_weather['weather_str'] = code_set[1]



# 本日の気温・湿度それぞれの折線グラフを作成
def create_temperature_relativehumidity_plot():
    # https://www.delftstack.com/ja/howto/matplotlib/how-to-change-the-size-and-format-of-a-figure-in-matplotlib/
    plt.rcParams["figure.figsize"] = (7.9, 3.5)
    # Figureを設定
    fig1 = plt.figure()
    plt.subplots_adjust(left=0.05, right=0.99, bottom=0.1, top=0.9)

    #################################################################################################
    # ax1 気温（本日）
    #################################################################################################
    # 本日の最高気温
    today_temperature_max = temperature_max_list[0]
    # 本日の最低気温
    today_temperature_min = temperature_min_list[0]

    ax1 = fig1.add_subplot(121)
    ax1.set_title("気温(°C)")
    # データを表示できる範囲
    ax1.set_xlim(0, 24)
    ax1.set_ylim(today_temperature_min-1, today_temperature_max+1)
    ax1.set_xticks(every_3_h)
    ax1.plot(x_24h, temperature_today24_list)
    # x軸に補助目盛線を設定
    ax1.grid(
        which = "major", axis = "x", color = "blue", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )
    # y軸に目盛線を設定
    ax1.grid(
        which = "major", axis = "y", color = "green", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )
    # 現在時刻の位置に縦線
    ax1.axvline(x=time_a_few_num, ymin=0.0, ymax=1.0, color="orange")

    #################################################################################################
    # ax2 湿度（本日）
    #################################################################################################
    # 本日の最高湿度
    today_relativehumidity_max = max(relativehumidity_today24_list)
    # 本日の最高湿度
    today_relativehumidity_min = min(relativehumidity_today24_list)
    ax2 = fig1.add_subplot(122)
    ax2.set_title("湿度(%)")
    # データを表示できる範囲
    ax2.set_xlim(0, 24) # ここ
    ax2.set_ylim(today_relativehumidity_min-1, today_relativehumidity_max+1)
    ax2.set_xticks(every_3_h)
    ax2.plot(x_24h, relativehumidity_today24_list, color='forestgreen')
    # x軸に補助目盛線を設定
    ax2.grid(
        which = "major", axis = "x", color = "blue", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )
    # y軸に目盛線を設定
    ax2.grid(
        which = "major", axis = "y", color = "green", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )
    # 現在時刻の位置に縦線
    ax2.axvline(x=time_a_few_num, ymin=0.0, ymax=1.0, color="orange")

    temperature_grelativehumidity_graph = Output_Graph()
    return temperature_grelativehumidity_graph


#################################################################################################
# ax3 気温・湿度（本日）
#################################################################################################
# 本日の気温・湿度の折線グラフを作成
def create_temperature_relativehumidity_mix_plot():
    fig = plt.figure()
    fig, ax3 = plt.subplots(figsize=(8, 4))
    plt.subplots_adjust(left=0.05, right=0.965, bottom=0.1, top=0.9)
    ax4 = ax3.twinx()
    ax3.set_title("気温(°C) / 湿度(%)")
    ax3.set_xlim(0, 24)
    ax3.set_xticks(x_24h)
    ax3.plot(x_24h, temperature_today24_list, label="気温(°C)")
    ax4.plot(x_24h, relativehumidity_today24_list, label="湿度(％)", color='forestgreen')

    handler3, label3 = ax3.get_legend_handles_labels()
    handler4, label4 = ax4.get_legend_handles_labels()
    ax3.legend(handler3 + handler4, label3 + label4, loc=2, borderaxespad=0)

    # x軸に補助目盛線を設定
    ax3.grid(
        which = "major", axis = "x", color = "blue", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )
    # y軸に目盛線を設定
    ax3.grid(
        which = "major", axis = "y", color = "green", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )

    ax3.axvline(x=time_a_few_num, ymin=0.0, ymax=1.0, color="orange")

    temperature_grelativehumidity_mix_graph = Output_Graph()
    return temperature_grelativehumidity_mix_graph

#################################################################################################
# ax4 気温・湿度（一週間分）
#################################################################################################
def create_temperature_relativehumidity_mix_week_plot():
    # x軸の目盛(本日の0時から7日後の23時までの168個（24*7))
    int_day = dt_now.day
    x_168h = []
    x_h_list = [i for i in range(0, 24)]
    for _ in range(7):
        for x_h in x_h_list:
            if x_h == 0:
                x_168h.append(str(int_day) + '/' + str(x_h))
                int_day += 1
            else:
                x_168h.append(x_h)

    fig = plt.figure()
    fig, ax5 = plt.subplots(figsize=(56, 4))
    plt.subplots_adjust(left=0.007, right=0.993, bottom=0.1, top=0.9)
    ax6 = ax5.twinx()
    ax5.set_title("気温(°C) / 湿度(%)")
    ax5.set_xlim(0, len(x_168h)-1)
    xticks_168 = [i for i in range(len(x_168h))]
    # x軸の目盛設定
    ax5.set_xticks(xticks_168)
    # x軸の目盛のラベル
    ax5.set_xticklabels(x_168h)
    ax5.plot(xticks_168, all_temperature_list, label="気温(°C)")
    ax6.plot(xticks_168, all_relativehumidity_list, label="湿度(％)", color='forestgreen')

    handler3, label3 = ax5.get_legend_handles_labels()
    handler4, label4 = ax6.get_legend_handles_labels()
    ax5.legend(handler3 + handler4, label3 + label4, loc=2, borderaxespad=0)

    # x軸に補助目盛線を設定
    ax5.grid(
        which = "major", axis = "x", color = "blue", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )
    # y軸に目盛線を設定
    ax5.grid(
        which = "major", axis = "y", color = "green", alpha = 0.8,
        linestyle = "--", linewidth = 1
    )

    ax5.axvline(x=time_a_few_num, ymin=0.0, ymax=1.0, color="orange")

    temperature_grelativehumidity_mix_week_graph = Output_Graph()
    return temperature_grelativehumidity_mix_week_graph


#################################################################################################
# プロットしたグラフを画像データとして出力するための関数
#################################################################################################
def Output_Graph():
	buffer = BytesIO()                   #バイナリI/O(画像や音声データを取り扱う際に利用)
	plt.savefig(buffer, format="png")    #png形式の画像データを取り扱う
	buffer.seek(0)                       #ストリーム先頭のoffset byteに変更
	img   = buffer.getvalue()            #バッファの全内容を含むbytes
	graph = base64.b64encode(img)        #画像ファイルをbase64でエンコード
	graph = graph.decode("utf-8")        #デコードして文字列から画像に変換
	buffer.close()
	return graph
