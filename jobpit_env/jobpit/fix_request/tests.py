from datetime import datetime
from django import template, test
from django.conf.urls import url
from django.http import response
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from django.views.generic.base import View
from .models import AttendanceFixRequests
from attendance.models import Attendances
import json

# Create your tests here.

class AttendanceFixRequestTest(TestCase):
    # クラスごとにテストデータとsetUpを設定
    fixtures = ['test_attendance_fix_records.json']
    def setUp(self):
        self.client = Client()
        self.client.login(
            username = 'testuser',
            password = 'samplesecret'
        )


    def test_not_exist_attendance_fix_request(self):
        '''
        attendancesにない打刻修正申請を行うためのテスト
        '''
        # 打刻修正のページを開く
        response = self.client.get('/fix_request/request')
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # contextの申請情報が存在しないこと
        # viewからtemplateに渡す辞書contextのキーfix_requestsを指定
        self.assertEqual(len(response.context['fix_requests']), 0)

        # 2021/5/21の出勤打刻の修正を申請する
        response = self.client.post('/fix_request/request', {
            'push_date':'2021-05-21',
            'push_type':'AT',
            'push_time':'09:30',
            'push_reason':'テスト用'
        })
        # 登録されたデータを取得
        attendance_request = AttendanceFixRequests.objects.get(
            # 文字列を日付型datetimeに変換
            revision_time__date = datetime.strptime('2021-05-21', '%Y-%m-%d')
        )

        self.assertEqual(response.status_code, 200)
        # attendance（attendance_id）が空であること
        self.assertIsNone(attendance_request.attendance)
        # print(vars(attendance_request))

        # 打刻修正のページを開く
        response = self.client.get('/fix_request/request')
        self.assertEqual(response.status_code, 200)
        # contextの申請情報が1つ存在すること
        self.assertEqual(len(response.context['fix_requests']), 1)


    def test_exist_attendance_fix_attendanced_request(self):
        '''
        attendancesにある打刻修正申請を行うためのテスト
        '''
        # 打刻修正のページを開く
        response = self.client.get('/fix_request/request')
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # contextの申請情報が存在しないこと
        self.assertEqual(len(response.context['fix_requests']), 0)
        # 2021/5/21の退勤打刻の修正を申請する
        response = self.client.post('/fix_request/request', {
            'push_date':'2021-05-21',
            'push_type':'AT',
            'push_time':'09:30',
            'push_reason':'テスト用'
        })
        self.assertEqual(response.status_code, 200)
        # 登録されたデータを取得
        attendance_request = AttendanceFixRequests.objects.get(
            revision_time__date = datetime.strptime('2021-05-21', '%Y-%m-%d')
        )
        # 該当するattendancesのデータが登録されていること
        self.assertEqual(
            attendance_request.revision_time, datetime.strptime('2021-05-21 09:30:00', '%Y-%m-%d %H:%M:%S')
        )
        self.assertEqual(attendance_request.stamp_type, 'AT')
        self.assertEqual(attendance_request.reason, 'テスト用')
        # 打刻修正のページを開く
        response = self.client.get('/fix_request/request')
        self.assertEqual(response.status_code, 200)
        # contextの申請情報が1つ存在すること
        self.assertEqual(len(response.context['fix_requests']), 1)


# 打刻修正申請一覧画面を開く、存在する申請IDの詳細画面を開く
class AttendacneFixAcceptionViewTest(TestCase):
    fixtures = ['test_attendance_fix_acception.json']
    def setUp(self):
        self.client = Client()
        self.client.login(
            username = 'testuser',
            password = 'samplesecret'
        )


    def test_attendance_fix_request_list(self):
        '''
        打刻修正申請一覧画面を開くテスト
        '''
        # スタッフユーザーに設定する
        # 同クラス内の関数を呼び出す場合は、selfを付ける
        set_staff()
        # self.client.is_staff = 1
        response = self.client.get('/fix_request/acception')
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # contextの申請情報が1つ存在すること
        self.assertEqual(len(response.context['fix_acceptions']), 1)


    def test_attendance_fix_acception_detail(self):
        '''
        存在する申請IDの詳細画面を開くテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/detail/1')
        self.assertEqual(response.status_code, 200)
        # contextの詳細情報が1つ存在すること
        # assertIsNotNoneの使い処？
        self.assertIsNotNone(response.context['request_detail'])


    def test_invalid_user_request_list(self):
        '''
        スタッフユーザーではないユーザーが打刻修正一覧画面を開くテスト
        '''
        response = self.client.get('/fix_request/acception')
        # ステータスコードが403であること
        self.assertEqual(response.status_code, 403)


    def test_invalid_user_acception_detali(self):
        '''
        スタッフユーザーではないユーザーが申請の詳細画面を開くテスト
        '''
        response = self.client.get('/fix_request/acception/detail/1')
        # ステータスコードが403であること
        self.assertEqual(response.status_code, 403)


# 修正申請に対する「承認」「却下」
class AcceptAttendanceFixRequestTest(TestCase):
    fixtures = ['test_attendance_fix_acception.json']
    def setUp(self):
        self.client = Client()
        # ログイン状態にする
        self.client.login(
            username = 'testuser',
            password = 'samplesecret'
        )


    def test_accept_fix_attendance_request(self):
        '''
        申請した修正を承認するテスト
        '''
        # スタッフユーザーの設定
        set_staff()
        # 打刻修正を承認する
        response = self.client.post('/fix_request/acception/push', {
            # 承認
            'btn_value':'accept',
            'request_id':1
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 承認の処理が完了したレスポンスが受け取れること
        # Ajaxの受け取り方
        response_body = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_body['result'], 'OK')

        # 変更したデータを取得する
        fix_requests = AttendanceFixRequests.objects.get(pk=1)
        # is_acceptedがTrue、「承認済」であること
        self.assertTrue(fix_requests.is_accepted)
        # 承認した日時が記録されていること
        self.assertIsNotNone(fix_requests.checked_time)


    def test_reject_fix_attendance_request(self):
        '''
        申請した修正を却下するテスト
        '''
        set_staff()
        # 打刻修正を却下する
        response = self.client.post('/fix_request/acception/push', {
            # 却下
            'btn_value':'reject',
            'request_id':1
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 却下の処理が完了したレスポンスが受け取れること
        response_body = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_body['result'], 'OK')

        # 変更したデータを取得する
        fix_requests = AttendanceFixRequests.objects.get(pk=1)
        # is_acceptedがFalse、「承認済」でないこと
        self.assertFalse(fix_requests.is_accepted)
        # 却下した日時が記録されていること
        self.assertIsNotNone(fix_requests.checked_time)


    def test_double_push_button(self):
        '''
        ボタンを2回押したときのテスト
        '''
        set_staff()
        # 1回目のボタン押下
        response = self.client.post('/fix_request/acception/push', {
            'btn_value':'accept',
            'request_id':1
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 却下の処理が完了したレスポンスが受け取れること
        response_body= json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_body['result'], 'OK')

        # 2回目のボタン押下
        response = self.client.post('/fix_request/acception/push', {
            'btn_value':'accept',
            'request_id':1
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 却下の処理が完了したレスポンスが受け取れること
        response_body = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_body['result'], 'acception_exists')


    def test_invalid_user_accept(self):
        '''
        スタッフユーザーではないユーザーが承認ボタンを押すテスト
        '''
        response = self.client.post('/fix_request/acception/push', {
            'btn_value':'accept',
            'request_id':1
        })
        # ステータスコードが403であること
        self.assertEqual(response.status_code, 403)


# 検索処理のテスト
class AcceptAttendanceFixSearchTest(TestCase):
    fixtures = ['test_attendance_fix_search_sort.json']
    def setUp(self):
        self.client = Client()
        # ログイン状態にする
        self.client.login(
            username = 'testuser1',
            password = 'samplesecret'
        )


    def test_not_is_staff_search(self):
        '''
        スタッフユーザーではないユーザーが検索ボタンを押すテスト
        '''
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '',
            'situation': ''
        })
        # アクセス権限なし
        self.assertEqual(response.status_code, 403)


    ########## ユーザー名 ##########


    def test_search_username(self):
        '''
        ユーザー名を入力して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser1',
            'ymd': '',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # ３件のデータを取得していること
        self.assertEqual(len(response.context['fix_acceptions']), 3)

        for fix_acception in response.context['fix_acceptions']:
            # 取得してデータのユーザー名がすべて「testuser1」であること
            self.assertEqual(fix_acception['user_name'], 'testuser1')


    def test_search_username_not_register(self):
        '''
        登録されていないユーザー名を入力して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser3',
            'ymd': '',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    ########## 申請日時 ##########


    def test_search_request_time(self):
        '''
        申請日時を指定して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '2021-05-11',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # ３件のデータを取得していること
        self.assertEqual(len(response.context['fix_acceptions']), 3)

        for fix_acception in response.context['fix_acceptions']:
            # 取得したデータの申請日時が「2021-05-11」であること
            self.assertTrue('2021-05-11' in fix_acception['request_time'])


    def test_search_request_time_not_register(self):
        '''
        登録されていない申請日時を指定して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '2021-05-31',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 0件のデータを取得していること
        self.assertEqual(len(response.context['fix_acceptions']), 0)


    ########## 状況 ##########


    def test_search_situation_not_checked(self):
        '''
        状況「未確認」を指定して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '',
            'situation': 'not_checked'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 1件のデータを取得していること
        self.assertEquals(len(response.context['fix_acceptions']), 1)

        for fix_acception in response.context['fix_acceptions']:
            # 取得したデータの状況が「not_checked」(未確認)であること
            self.assertEquals(fix_acception['request_status'], 'not_checked')


    def test_search_situation_accepted(self):
        '''
        状況「承認」を指定して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '',
            'situation': 'accepted'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # ３件のデータを取得していること
        self.assertEquals(len(response.context['fix_acceptions']), 3)

        for fix_acception in response.context['fix_acceptions']:
            # 取得したデータの状況が「accepted」(承認)であること
            self.assertEquals(fix_acception['request_status'], 'accepted')


    def test_search_situation_rejected(self):
        '''
        状況「却下」を指定して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '',
            'situation': 'rejected'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 1件のデータを取得していること
        self.assertEquals(len(response.context['fix_acceptions']), 1)

        for fix_acception in response.context['fix_acceptions']:
            # 取得したデータの状況が「rejected」(却下)であること
            self.assertEquals(fix_acception['request_status'], 'rejected')


    def test_search_situation_test(self):
        '''
        状況のvalueにシステム上存在しないsituationを指定して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '',
            'situation': 'test'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    ########## ユーザー名と申請日時 ##########


    def test_search_testuser1_20210511(self):
        '''
        ユーザー名、申請日時を入力して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser1',
            'ymd': '2021-05-11',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 2件のデータを取得していること
        self.assertEquals(len(response.context['fix_acceptions']), 2)

        for fix_acception in response.context['fix_acceptions']:
            for element in fix_acception:
                if element == 'name':
                    # 取得したデータのユーザー名が「testuser1」であること
                    self.assertEquals(fix_acception[element], 'testuser1')
                elif element == 'request_time':
                    # 取得したデータの申請日時に「2021-05-11」が含まれていること
                    self.assertTrue('2021-05-11' in fix_acception[element])


    def test_search_testuser1_20210601(self):
        '''
        登録されたユーザー名、登録されていない申請日時を入力して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser1',
            'ymd': '2021-06-01',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること
        self.assertEquals(len(response.context['fix_acceptions']), 0)


    def test_search_testuser3_20210511(self):
        '''
        登録されていないユーザー名、登録された申請日時を入力して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser3',
            'ymd': '2021-05-11',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    def test_search_testuser3_20210601(self):
        '''
        登録されていないユーザー名、登録されていない申請日時を入力して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser3',
            'ymd': '2021-06-01',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    ########## ユーザー名と状況 ##########


    def test_search_testuser1_not_checked(self):
        '''
        ユーザー名、状況を入力(存在する組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser1',
            'ymd': '',
            'situation': 'not_checked'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 2件のデータを取得していること
        self.assertEquals(len(response.context['fix_acceptions']), 1)

        for fix_acception in response.context['fix_acceptions']:
            for element in fix_acception:
                if element == 'name':
                    # 取得したデータのユーザー名が「testuser1」であること
                    self.assertEquals(fix_acception[element], 'testuser1')
                elif element == 'request_time':
                    # 取得したデータの申請日時に「2021-05-11」が含まれていること
                    self.assertTrue('2021-05-11' in fix_acception[element])


    def test_search_testuser1_situation_test(self):
        '''
        ユーザー名、システム上登録されていない状況を入力(存在しない組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser1',
            'ymd': '',
            'situation': 'test'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    def test_search_testuser2_not_checked(self):
        '''
        登録されていないユーザー名、登録されている状況を入力(存在しない組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser2',
            'ymd': '',
            'situation': 'not_checked'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること
        self.assertEquals(len(response.context['fix_acceptions']), 0)


    def test_search_testuser3_tets(self):
        '''
        登録されていないユーザー名、システム上存在しない状況を入力(存在しない組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser3',
            'ymd': '',
            'situation': 'test'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    ########## 申請日時と状況 ##########


    def test_search_20210511_not_checked(self):
        '''
        申請日時、状況「not_checked」(未確認)を入力(存在する組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '2021-05-11',
            'situation': 'not_checked'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 2件のデータを取得していること
        self.assertEquals(len(response.context['fix_acceptions']), 1)

        for fix_acception in response.context['fix_acceptions']:
            for element in fix_acception:
                if element == 'request_time':
                    # 取得したデータの申請日時に「2021-05-11」が含まれていること
                    self.assertTrue('2021-05-11' in fix_acception[element])
                elif element == 'situation':
                    # 状況が「not_checked」であること
                    self.assertEquals(fix_acception[element] == 'not_checked')


    def test_search_20210511_test(self):
        '''
        申請日時、システム上存在しない状況を入力(存在しない組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '2021-05-11',
            'situation': 'test'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    def test_search_20210601_not_checked(self):
        '''
        登録されていない申請日時、登録されている状況を入力(存在しない組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '2021-06-01',
            'situation': 'not_checked'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること
        self.assertEquals(len(response.context['fix_acceptions']), 0)


    def test_search_20210601_tets(self):
        '''
        登録されていない申請日時、システム上存在しない状況を入力(存在しない組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': '',
            'ymd': '2021-06-01',
            'situation': 'test'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること:キー「fix_acceptions」がないこと
        self.assertFalse('fix_acceptions' in response.context)


    ########## ユーザー名と申請日時と状況 ##########


    def test_search_testuser1_20210511_not_checked(self):
        '''
        登録されているユーザー名、登録されている申請日時、登録されている状況を入力(存在する組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser1',
            'ymd': '2021-05-11',
            'situation': 'not_checked'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること
        self.assertEquals(len(response.context['fix_acceptions']), 1)

        for fix_acception in response.context['fix_acceptions']:
            for element in fix_acception:
                if element == 'name':
                    # 取得したデータのユーザー名が「testuser1」であること
                    self.assertEquals(fix_acception[element], 'testuser1')
                elif element == 'request_time':
                    # 取得したデータ申請日時にが「2021-05-11」が含まれていること
                    self.assertTrue('2021-05-11' in fix_acception[element])
                elif element == 'situation':
                    # 取得したデータの状況が「not_checked」であること
                    self.assertEquals(fix_acception[element], 'not_checked')


    def test_search_testuser1_20210601_not_checked(self):
        '''
        登録されているユーザー名、登録されていない申請日時、登録されていない状況を入力(存在しない組み合わせ)して検索ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/search', {
            'name': 'testuser1',
            'ymd': '2021-06-01',
            'situation': 'not_checked'
        })
        # ステータスコードが200であること
        self.assertEquals(response.status_code, 200)
        # 取得データが0件であること
        self.assertEquals(len(response.context['fix_acceptions']), 0)


# ソート処理のテスト
class AcceptAttendanceFixSortTest(TestCase):
    fixtures = ['test_attendance_fix_search_sort.json']
    def setUp(self):
        self.client = Client()
        # ログイン状態にする
        self.client.login(
            username = 'testuser1',
            password = 'samplesecret'
        )


    def test_not_is_staff_sort_des(self):
        '''
        スタッフユーザーではないユーザーが「新しい順」ボタンを押すテスト
        '''
        response = self.client.get('/fix_request/acception/sort-des', {
            'name': '',
            'ymd': '',
            'situation': ''
        })
        # アクセス権限なし
        # ステータスコードが403であること
        self.assertEqual(response.status_code, 403)


    def test_not_is_staff_sort_des(self):
        '''
        スタッフユーザーではないユーザーが「古い順」ボタンを押すテスト
        '''
        response = self.client.get('/fix_request/acception/sort-asc', {
            'name': '',
            'ymd': '',
            'situation': ''
        })
        # アクセス権限なし
        # ステータスコードが403であること
        self.assertEqual(response.status_code, 403)


    def test_sort_des(self):
        '''
        「新しい順」ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/sort-des', {
            'name': '',
            'ymd': '',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 取得データが5件であること
        self.assertEqual(len(response.context['fix_acceptions']), 5)

        count = 0
        for _ in response.context['fix_acceptions']:
            if count == 4:
                break
            # 申請日付が新しい順番であること
            self.assertTrue(
                response.context['fix_acceptions'][count]['request_time'] >= response.context['fix_acceptions'][count+1]['request_time']
            )
            count  = count + 1


    def test_sort_des(self):
        '''
        「古い順」ボタンを押すテスト
        '''
        set_staff()
        response = self.client.get('/fix_request/acception/sort-asc', {
            'name': '',
            'ymd': '',
            'situation': ''
        })
        # ステータスコードが200であること
        self.assertEqual(response.status_code, 200)
        # 取得データが5件であること
        self.assertEqual(len(response.context['fix_acceptions']), 5)

        count = 0
        for _ in response.context['fix_acceptions']:
            if count == 4:
                break
            # 申請日付が古い順番であること
            self.assertTrue(
                response.context['fix_acceptions'][count]['request_time'] <= response.context['fix_acceptions'][count+1]['request_time']
            )
            count  = count + 1


# スタッフユーザーに設定する
def set_staff():
    staff_user = User.objects.get(pk=1)
    staff_user.is_staff = True
    staff_user.save()


