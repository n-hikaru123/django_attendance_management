from django.urls import path
from .views import (
    FixAttendanceRequestView,
    AttendanceAcceptionView,
    AcceptionDetailView,
    PushAcceptionView,
    SearchAcceptionView,
    SortAcceptionView,
    DeleteAcceptionView
)

urlpatterns = [
    # 打刻修正
    # /fix_request/request
    path('request', FixAttendanceRequestView.as_view(), name='fix_request'),
    # get:打刻修正申請一覧画面を開く
    # post：打刻承認
    # /fix_request/acception
    path('acception', AttendanceAcceptionView.as_view(), name='fix_acception'),
    # 修正申請詳細
    # /fix_request/acception/detail/<int:request_id>
    # <int:request_id>:キーワード引数
    path('acception/detail/<int:request_id>', AcceptionDetailView.as_view(), name='acception_detail'),
    # fix_request/acception/push
    path('acception/push', PushAcceptionView.as_view(), name='push_acception'),
    # fix_request/acception/search
    path('acception/search', SearchAcceptionView.as_view(), name='acception_search'),

    # fix_request/acception/sort-asc_target_time　対象日 昇順 古い順
    path('acception/sort-asc_target_time', SortAcceptionView.as_view(), name='acception_sort_asc_by_target_time'),
    # fix_request/acception/sort-des_target_time　対象日 降順 新しい順
    path('acception/sort-des_target_time', SortAcceptionView.as_view(), name='acception_sort_des_by_target_time'),

    # fix_request/acception/sort-asc_request_time　申請日 昇順 古い順
    path('acception/sort-asc_request_time', SortAcceptionView.as_view(), name='acception_sort_asc_by_request_time'),
    # fix_request/acception/sort-des_request_time　申請日 降順 新しい順
    path('acception/sort-des_request_time', SortAcceptionView.as_view(), name='acception_sort_des_by_request_time'),

    # 修正申請の削除
    # fix_request/delete
    path('acception/delete', DeleteAcceptionView.as_view(), name='fix_request_delete')

]