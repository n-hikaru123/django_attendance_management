from django.http import request
from django.http.response import JsonResponse
from ..models import AttendanceFixRequests
from django.contrib.auth.models import User
from datetime import datetime
from django.shortcuts import redirect


class SearchSortAcceptionService():

    # 検索条件に応じた検索結果を取得「検索」
    def search(self, *args, **kwargs):

        # 並び替えのボタンを押下した場合、並び替えボタンに従う
        if 'sort_rule' in kwargs:
            sort_rule = kwargs['sort_rule']
        # 初期表示又は検索ボタンを押下した場合、申請の修正依頼が新しい順番
        else:
            sort_rule = '-request_time'

        name = kwargs['name']
        target_ymd = kwargs['target_ymd']
        request_ymd = kwargs['request_ymd']
        situation = kwargs['situation']

        conditions = {}

        # ユーザー名
        if not name == '':
            is_user = User.objects.filter(username=name).exists()
            if is_user:
                user_info = User.objects.get(username=name)
                # まとめる
                if user_info:
                    conditions['user_id'] = user_info.id
            else:
                return 'not_user'

        # 対象日
        if not target_ymd == '':
            target_ymd = datetime.strptime(target_ymd, '%Y-%m-%d')
            conditions['revision_time__date'] = target_ymd

        # 申請日時
        # キーがない場合も対応する必要あり
        if not request_ymd == '':
            request_ymd = datetime.strptime(request_ymd, '%Y-%m-%d')
            conditions['request_time__date'] = request_ymd

        # 確認状況
        if not situation == '':
            if situation == 'accepted':
                # situation = 1
                conditions['is_accepted'] = 1
            elif situation == 'not_checked':
                conditions['is_accepted'] = 0
                # checked_timeカラムの値が入っていないものを指定
                conditions['checked_time__isnull'] = True
            elif situation == 'rejected':
                conditions['is_accepted'] = 0
                # checked_timeカラムの値が入っているものを指定
                conditions['checked_time__isnull'] = False
            else:
                return 'not_status_situation'

        fix_requests = AttendanceFixRequests.objects.order_by(sort_rule).filter(
            **conditions, deleted_at__isnull=True
        )
        return fix_requests


    # 新しい順又は古い順が押下された場合
    def sort(self, *args, **kwargs):
        name = kwargs['name']
        target_ymd = kwargs['target_ymd']
        request_ymd = kwargs['request_ymd']
        situation = kwargs['situation']
        url = kwargs['url']

        if url == '/fix_request/acception/sort-des_request_time':
            sort_rule = 'request_time'
        elif url == '/fix_request/acception/sort-asc_request_time':
            sort_rule = '-request_time'
        elif url == '/fix_request/acception/sort-des_target_time':
            sort_rule = 'revision_time'
        elif url == '/fix_request/acception/sort-asc_target_time':
            sort_rule = '-revision_time'

        fix_requests = self.search(name=name, target_ymd=target_ymd, request_ymd=request_ymd, situation=situation, sort_rule=sort_rule)

        return fix_requests
