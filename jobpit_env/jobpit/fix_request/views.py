from django.http.response import JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404, render, redirect
from .models import AttendanceFixRequests
from attendance.models import Attendances
from datetime import datetime
from .service.SearchSortAcceptionService import SearchSortAcceptionService
import json
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# 打刻修正画面
class FixAttendanceRequestView(LoginRequiredMixin, TemplateView):
    template_name = 'fix_request.html'
    login_url = '/accounts/login/'

    # 打刻修正画面を開く
    def get(self, request, *args, **kwargs):
        # ユーザーの打刻修正申請一覧を取得
        fix_requests = AttendanceFixRequests.objects.filter(
            user = request.user
        )

        resp_params = []
        # 表示用に整形
        for fix_request in fix_requests:
            print(vars(fix_request))
            print(fix_request.get_stamp_type_display())
            # 状況判定
            request_status = SituationJudgment(fix_request)
            resp_param = {
                'date':fix_request.revision_time.strftime('%Y/%m/%d'),
                # fix_request.stamp_type = 'AT'のときに表示用の値を取得するときにはfix_request.get_stamp_type_display() = 'attendance'となる。
                'stamp_type':fix_request.get_stamp_type_display(),
                'revision_time': fix_request.revision_time.strftime('%H:%M'),
                'request_status':request_status
            }
            resp_params.append(resp_param)

        context = {'fix_requests':resp_params}

        return self.render_to_response(context)


    # 打刻修正の申請
    def post(self, request, *args, **kwargs):

        # 画面から通常に修正申請を行う場合
        if not 'request_key' in kwargs:
        # リクエストパラメータを取得
            push_date = request.POST.get('push_date')
            push_type = request.POST.get('push_type')
            push_time = request.POST.get('push_time')
            push_reason = request.POST.get('push_reason')
            user = request.user
        # サンプルデータ作成時
        else:
            print(kwargs)
            # AttendanceFixRequestsの取得条件のuserはUserインスタンスを入れる
            print(kwargs['request_key']['push_date'])
            user = User(kwargs['request_key']['user'])
            push_date = kwargs['request_key']['push_date']
            push_type = kwargs['request_key']['push_type']
            push_time = kwargs['request_key']['push_time']
            push_reason = kwargs['request_key']['push_reason']

        fix_datetime = '{}T{}'.format(push_date, push_time)

        # get()はモデルインスタンスを返すのでexists()はエラーになる
        # https://qiita.com/shonansurvivors/items/12b087cf5ab591273c8c
        # exists()はTrue又はFalseを返す
        is_attendanced = Attendances.objects.filter(
            user = user,
            # https://qiita.com/miler0528/items/b99310c16bc2056a4412 __date
            attendance_time__date = datetime.strptime(push_date, '%Y-%m-%d')
        ).exists()
        print(is_attendanced)

        # 打刻修正のデータを登録する
        # get()メソッドは、モデルのオブジェクトを1件だけ取得するメソッド
        if is_attendanced:
            attendance = Attendances.objects.get(
                user = user,
                attendance_time__date = datetime.strptime(push_date, '%Y-%m-%d')
            )
            print(vars(attendance))
            # テーブルに保存用のオブジェクトを作る
            fix_request = AttendanceFixRequests(
                user = user,
                attendance = attendance, # attendance_id に値が入る
                stamp_type = push_type,
                reason = push_reason,
                revision_time = datetime.strptime(fix_datetime, '%Y-%m-%dT%H:%M')
            )
        else:
            fix_request = AttendanceFixRequests(
                user = user,
                stamp_type = push_type,
                reason = push_reason,
                revision_time = datetime.strptime(fix_datetime, '%Y-%m-%dT%H:%M')
            )
        fix_request.save()

        # ajaxから呼び出された場合、JsonResponseで返す
        return JsonResponse({'status':'OK'})


# 打刻承認画面を開く
# UserPassesTestMixin:管理者権限のある者だけが開くことができるようにする
# ↓
# test_func():特定の条件に当てはまる人だけアクセスできるようにする
class AttendanceAcceptionView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'request_acception.html'
    login_url = '/accounts/login/'
    def test_func(self):
        user = self.request.user
        return user.is_staff

    def get(self, request, *args, **kwargs):
        fix_requests = AttendanceFixRequests.objects.order_by('-request_time').filter(deleted_at__isnull=True)

        request_list = []
        id_list = []
        for fix_request in fix_requests:
            # 状況判定
            request_status = SituationJudgment(fix_request)
            request_data = {
                # ここではfix_request.idと同じ
                'id':fix_request.pk,
                'user_name':fix_request.user.username,
                'target_date':fix_request.revision_time.strftime('%Y-%m-%d'),
                'request_type':fix_request.stamp_type,
                # strftime(): 日付、時間から文字列への変換
                'request_time':fix_request.request_time.strftime('%Y-%m-%d %H:%M:%S'),
                'request_status':request_status
            }
            request_list.append(request_data)
            id_list.append(fix_request.pk)

        paginate = Paginate()
        page_obj = paginate.paginate_queryset(request, request_list, 8)

        # 取得データ数
        count = len(request_list)

        context = {
            'fix_acceptions':page_obj,
            'count':count,
            'id_list':id_list
        }

        return self.render_to_response(context)


# ページング処理
class Paginate:
    def paginate_queryset(self, request, queryset, count):
        paginator = Paginator(queryset, count)
        page = request.GET.get('page')

        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            # page()に渡される値が整数でないとき
            page_obj = paginator.page(1)
        except EmptyPage:
            # 空のページ（最大ページ数を超えたときには空のページになる）を指定したとき
            page_obj = paginator.page(paginator.num_pages)

        print(page_obj)
        return page_obj


# 打刻承認画面:申請詳細画面を開く
class AcceptionDetailView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'acception_detail.html'
    login_url = '/accounts/login/'
    def test_func(self):
        user = self.request.user
        return user.is_staff

    def get(self, request, *args, **kwargs):
        # キーワード引数の受け取り方
        # print(self.kwargs['request_id'])
        request_id = self.kwargs['request_id']
        """
        get_object_or_404:Djangoが提供するショートカットの一つ
        Attendances.objects.get()でデータを取得して条件に当てはまるデータがあればその値をセットすることができて、
        存在しなければステータスコード404をgetメソッドに渡します。
        """
        fix_request = get_object_or_404(AttendanceFixRequests, pk=request_id)
        print(vars(fix_request))
        context = {'request_detail':fix_request}
        return self.render_to_response(context)


# 打刻承認画面:申請承認
class PushAcceptionView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'acception_detail.html'
    login_url = '/accounts/login/'
    def test_func(self):
        user = self.request.user
        return user.is_staff

    def post(self, request, *args, **kwargs):
        btn_value = request.POST.get('btn_value')
        request_id = request.POST.get('request_id')
        fix_request = AttendanceFixRequests.objects.get(pk=request_id)
        print(vars(fix_request))

        # インスタンス名.モデル名で互いにアクセス
        # fix_requestに紐づく「attendanceのオブジェクト」を取得
        print('#####################')
        if fix_request.attendance:
            print(vars(fix_request.attendance))
        print('#####################')

        # 確認日時が存在するときはデータ更新を行わない
        if fix_request.checked_time:
            return JsonResponse({'result':'acception_exists'})

        fix_request.checked_time = datetime.now()
        print(vars(fix_request))

        # 承認の処理
        if btn_value == 'accept':

            # 対象日の出勤情報が存在しないが、退勤の修正申請を「承認」ボタンを押下した場合、承認処理を行わない
            is_attendanced = Attendances.objects.filter(
                user_id = fix_request.user_id,
                attendance_time__date = fix_request.revision_time
            ).exists()
            print(is_attendanced)

            if fix_request.stamp_type == 'LE' and is_attendanced == False:
                print('対象日の出勤情報が存在しないが、退勤の修正申請を「承認」ボタンを押下した場合、承認処理を行わない')
                return JsonResponse({'result':'NG'})

            # 承認されたらfix_requestに紐づくattendancesのレコードを更新させる
            fix_request.is_accepted = True

            # 既に打刻されたものに対して修正申請している場合
            # 出勤記録は存在するが、退勤記録がない(NULL)場合、「fix_request.attendance」ではデータ取得不可
            # 　「'attendance_id': None」となる
            if fix_request.attendance or is_attendanced == True:
                if fix_request.stamp_type == 'AT':
                    # 出勤時間の修正
                    # fix_requestに紐づくattendancesのレコードを取得
                    # print(fix_request.attendance.attendance_time)
                    fix_request.attendance.attendance_time = fix_request.revision_time
                elif fix_request.stamp_type == 'LE':
                    # 退勤時間の修正
                    # fix_request.attendance.leave_time = fix_request.revision_time
                    fix_request.attendance = Attendances.objects.get(
                        user_id = fix_request.user_id,
                        attendance_time__date = fix_request.revision_time
                    )
                    fix_request.attendance.leave_time = fix_request.revision_time

            # 打刻されていないものに対して修正申請している場合
            else:
                if fix_request.stamp_type == 'AT':
                    """
                    Model()などでマニュアル的にモデルからオブジェクトを生成する場合は、
                    オブジェクトの保存は開発者が自由に行うことが出来ます。
                    https://yu-nix.com/blog/2020/11/27/django-create/
                    """
                    # fix_requestに紐づくAttendancesモデルのオブジェクトを新規で作成
                    fix_request.attendance = Attendances(
                        user = fix_request.user,
                        attendance_time = fix_request.revision_time
                    )
                elif fix_request.stamp_type == 'LE':
                    print('この処理は現状行わない')
                    return JsonResponse({'result':'OK'})
            # 通常の勤怠履歴のテーブル(attendance)にデータを保存
            fix_request.attendance.save()

        # 却下の場合 通常の勤怠履歴のテーブルの更新処理は行わない
        elif btn_value == 'reject':
            fix_request.is_accepted = False

        # 打刻修正テーブルを更新
        fix_request.save()

        return JsonResponse({'result':'OK'})


        """
        インスタンス名.モデル名で互いにアクセス
        fix_request.attendance:fix_requestに紐づくattendancesのレコード(オブジェクト)

        fix_request:fix_requestのオブジェクト
        """


# 打刻承認画面での検索
class SearchAcceptionView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'request_acception.html'
    login_url = '/accounts/login/'
    def test_func(self):
        user = self.request.user
        return user.is_staff

    def get(self, request, *args, **kwargs):
        # 検索結果の1ページ目
        if not 'page' in request.GET:
            # セッションに保存
            request.session['name'] = request.GET.get('name')
            request.session['target_ymd'] = request.GET.get('target_ymd')
            request.session['request_ymd'] = request.GET.get('request_ymd')
            request.session['situation'] = request.GET.get('situation')

        # 2ページ以降は検索条件はセッションに保存されている
        name = request.session['name']
        target_ymd = request.session['target_ymd']
        request_ymd = request.session['request_ymd']
        situation = request.session['situation']

        # 検索条件に何も入力されていない場合、申請一覧画面にリダイレクト
        if name == '' and target_ymd == '' and request_ymd == '' and situation == '':
            return redirect('fix_acception')

        # SearchSortAcceptionServiceクラスのインスタンスを作成
        search_sort_acception_service = SearchSortAcceptionService()
        # SearchSortAcceptionServiceクラスのsearch関数を呼び出す
        # 検索条件に応じた値を取得
        search_fix_requests = search_sort_acception_service.search(name=name, target_ymd=target_ymd, request_ymd=request_ymd, situation=situation)

        # 取得データの件数
        if search_fix_requests == 'not_user':
            count = 0
        else:
            count = len(search_fix_requests)

        # 取得データを表示用に成形
        page_obj = displayMolding(request, search_fix_requests)

        context = {
            'fix_acceptions':page_obj,
            'input_name':name,
            'input_target_ymd':target_ymd,
            'input_request_ymd':request_ymd,
            'input_situation':situation,
            'count':count
        }

        return self.render_to_response(context)


# 打刻承認画面での並び替え
class SortAcceptionView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'request_acception.html'
    login_url = '/accounts/login/'
    def test_func(self):
        user = self.request.user
        return user.is_staff

    def get(self, request, *args, **kwargs):
        if not 'page' in request.GET:
            # セッションに保存
            request.session['name'] = request.GET.get('name')
            request.session['target_ymd'] = request.GET.get('target_ymd')
            request.session['request_ymd'] = request.GET.get('request_ymd')
            request.session['situation'] = request.GET.get('situation')

        name = request.session['name']
        target_ymd = request.session['target_ymd']
        request_ymd = request.session['request_ymd']
        situation = request.session['situation']

        # URLを取得
        url = request.path
        # SearchSortAcceptionServiceクラスのインスタンスを作成
        search_sort_acception_service = SearchSortAcceptionService()
        # 並び替えた検索結果を取得
        sort_fix_requests = search_sort_acception_service.sort(name=name, target_ymd=target_ymd, request_ymd=request_ymd, situation=situation, url=url)

        # 取得データの件数
        if sort_fix_requests == 'not_user':
            count = 0
        else:
            count = len(sort_fix_requests)

        # 取得データを表示用に成形
        page_obj = displayMolding(request, sort_fix_requests)

        context = {
            'fix_acceptions':page_obj,
            'input_name':name,
            'input_target_ymd':target_ymd,
            'input_request_ymd':request_ymd,
            'input_situation':situation,
            'count':count
        }

        return self.render_to_response(context)


# 検索・ソートの取得データを表示用に成形
def displayMolding(request, fix_requests):
    # 検索対象のユーザーが存在しない場合
    if fix_requests == 'not_user':
        return None
    # 存在しない状況のステータスが検索条件にある場合
    if fix_requests == 'not_status_situation':
        return None

    search_fix_requests_list = []
    for search_fix_request in fix_requests:
        # 状況判定
        request_status = SituationJudgment(search_fix_request)
        user_name = User.objects.values_list('username', flat=True).get(
            id = search_fix_request.user_id
        )
        search_fix_request_data = {
            'id':search_fix_request.id,
            'user_name':user_name,
            'target_date':search_fix_request.revision_time.strftime('%Y-%m-%d'),
            'request_type':search_fix_request.stamp_type,
            'request_time':search_fix_request.request_time.strftime('%Y-%m-%d %H:%M:%S'),
            'request_status':request_status
        }

        search_fix_requests_list.append(search_fix_request_data)

    paginate = Paginate()
    context = paginate.paginate_queryset(request, search_fix_requests_list, 8)

    return context


# 状況判定
def SituationJudgment(fix_request):
    # 承認済でない、かつ、申請確認時刻に値がない:申請未確認
    if not fix_request.is_accepted and not fix_request.checked_time:
        request_status = 'not_checked'
    # 承認済でない、かつ、申請確認時刻に値がある:申請が拒否された
    elif not fix_request.is_accepted and fix_request.checked_time:
        request_status = 'rejected'
    # 申請が認めらた
    else:
        request_status = 'accepted'

    return request_status


# 修正申請を削除する処理
class DeleteAcceptionView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'request_acception.html'
    login_url = '/accounts/login/'
    def test_func(self):
        user = self.request.user
        return user.is_staff

    def post(self, request, *args, **kwargs):
        # 配列のデータを受け取る場合:getlist、キー名に[]を付ける
        delete_list = request.POST.getlist('delete_list[]')

        # 論理削除
        for fix_request_id in delete_list:
            fix_request = AttendanceFixRequests.objects.get(id=fix_request_id)
            fix_request.delete()

        return JsonResponse({'result':'DeleteAcceptionView'})

