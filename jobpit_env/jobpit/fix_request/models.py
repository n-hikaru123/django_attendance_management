from django.db import models
# from アプリケーション名.models import クラス名
# 参考：INSTALLED_APPS
from django.contrib.auth.models import User
# https://qiita.com/keisukesato-ac/items/6213925bb167c25cc37c　警告
from attendance.models import Attendances
# 論理削除
from django_boost.models.mixins import LogicalDeletionMixin

class AttendanceFixRequests(LogicalDeletionMixin):
    # ログイン情報で管理しているユーザーID
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # 修正を行う勤怠情報のID
    attendance = models.ForeignKey(Attendances, on_delete=models.CASCADE, null=True)
    # 修正理由
    reason = models.CharField(max_length=255)
    # 打刻の種別
    # choicesはタプルの1番目の値がDBに挿入されるので、max_lengthが2となる
    stamp_type = models.CharField(
        max_length=2,
        choices= [
            ('AT', 'attendance'),
            ('LE', 'leave'),
        ]
    )
    # 受諾済みかどうか
    # models.BooleanFieldはデータ挿入時はNullにできないため、デフォルト値を指定する必要がある。
    is_accepted = models.BooleanField(default=False)
    # 修正時刻
    revision_time = models.DateTimeField()
    # 申請時刻
    # auto_now_add=True：新たにオブジェクトが作成されたときに自動的に日時が記録される
    request_time = models.DateTimeField(auto_now_add=True)
    # 申請確認時刻
    checked_time = models.DateTimeField(null=True)

