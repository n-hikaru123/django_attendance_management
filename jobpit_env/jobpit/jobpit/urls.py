"""jobpit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django import urls
from django.contrib import admin
from django.urls import path, include
# from django.views.generic.base import TemplateView ※１

urlpatterns = [
    path('admin/', admin.site.urls),
    # URLのパスでaccounts/を指定するとDjangoが標準で提供している認証周りのURLをまとめて呼び出しています。
    # include関数により、アプリケーション内のurls.pyを連結することができます。
    # django.contrib.auth.urls:Djangoの便利機能
    path('accounts/', include('django.contrib.auth.urls')),
    # path(URL, include(アプリケーションフォルダ名.urls))
    # include('accounts.urls')というのは、アプリケーションフォルダ(accounts)内のaccounts/urls.pyを呼び出しています。
    path('accounts/', include('accounts.urls')),
    # viewで処理を行うことなく、templateを呼び出している
    # path('', TemplateView.as_view(template_name='home.html'), name='home'), ※１
    path('', include('attendance.urls')),
    path('fix_request/', include('fix_request.urls')),
    path('graph_attendances/', include('graph_Attendances_info.urls')),
    path('data/', include('insert_data.urls'))
]
