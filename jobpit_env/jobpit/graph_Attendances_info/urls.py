from django.urls import path
from django.contrib.auth.models import User
from .views import AttendancesGraphView

urlpatterns = [
    # graph_attendances/{user_id}
    path('<int:user_id>', AttendancesGraphView.as_view(), name='attendances_graph')
]