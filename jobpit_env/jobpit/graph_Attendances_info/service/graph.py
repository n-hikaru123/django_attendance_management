import sys
from turtle import color, left
import matplotlib
matplotlib.use('Agg') # Matplotlibクラッシュ回避
import matplotlib.pyplot as plt
import base64
from io import BytesIO
from matplotlib import dates as mdates
from datetime import datetime as dt
import japanize_matplotlib

#プロットしたグラフを画像データとして出力するための関数
def Output_Graph():

	buffer = BytesIO()                   #バイナリI/O(画像や音声データを取り扱う際に利用)
	plt.savefig(buffer, format="png")    #png形式の画像データを取り扱う
	buffer.seek(0)                       #ストリーム先頭のoffset byteに変更
	img   = buffer.getvalue()            #バッファの全内容を含むbytes
	graph = base64.b64encode(img)        #画像ファイルをbase64でエンコード
	graph = graph.decode("utf-8")        #デコードして文字列から画像に変換
	buffer.close()
	return graph


# #グラフをプロットするための関数
def Plot_Graph(arr_x_all_days, arr_y_times, arr_month, arr_total_time, pie_data):

	# 描画キャンパスを何行に分割するのかを1番目の引数で指定し、
	# 何列に分割するのかを2番目の引数で指定する。
	# 3番目の引数に、これからグラフを描くのに利用するサブ領域の番号を入れる。

	fig = plt.figure(figsize=(12, 50))
	gs = fig.add_gridspec(12, 2, width_ratios=[3, 1], height_ratios=[1,1,1,1,1,1,1,1,1,1,1,1])
	plt.subplots_adjust(hspace=1)
	# gs[行, 列]
	bar_ax1 = fig.add_subplot(gs[0, 0])
	bar_ax2 = fig.add_subplot(gs[1, 0])
	bar_ax3 = fig.add_subplot(gs[2, 0])
	bar_ax4 = fig.add_subplot(gs[3, 0])
	bar_ax5 = fig.add_subplot(gs[4, 0])
	bar_ax6 = fig.add_subplot(gs[5, 0])
	bar_ax7 = fig.add_subplot(gs[6, 0])
	bar_ax8 = fig.add_subplot(gs[7, 0])
	bar_ax9 = fig.add_subplot(gs[8, 0])
	bar_ax10 = fig.add_subplot(gs[9, 0])
	bar_ax11 = fig.add_subplot(gs[10, 0])
	bar_ax12 = fig.add_subplot(gs[11, 0])
	pie_ax1 = fig.add_subplot(gs[0, 1])
	pie_ax2 = fig.add_subplot(gs[1, 1])
	pie_ax3 = fig.add_subplot(gs[2, 1])
	pie_ax4 = fig.add_subplot(gs[3, 1])
	pie_ax5 = fig.add_subplot(gs[4, 1])
	pie_ax6 = fig.add_subplot(gs[5, 1])
	pie_ax7 = fig.add_subplot(gs[6, 1])
	pie_ax8 = fig.add_subplot(gs[7, 1])
	pie_ax9 = fig.add_subplot(gs[8, 1])
	pie_ax10 = fig.add_subplot(gs[9, 1])
	pie_ax11 = fig.add_subplot(gs[10, 1])
	pie_ax12 = fig.add_subplot(gs[11, 1])

	# 棒グラフ
	bar_axes = [
		bar_ax1, bar_ax2, bar_ax3, bar_ax4, bar_ax5, bar_ax6, bar_ax7, bar_ax8, bar_ax9, bar_ax10, bar_ax11, bar_ax12
	]

	for (xlist, ylist, month, total_time, ax) in zip(arr_x_all_days, arr_y_times, arr_month, arr_total_time, bar_axes):

		xlist = [dt.strptime(d, '%Y-%m-%d') for d in xlist]
		ylist = [dt.strptime(d, '%H:%M:%S') for d in ylist]

		# FigureオブジェクトにAxesオブジェクトが属している
		# AxesオブジェクトにはAxisオブジェクトが属している

		ax.bar(xlist, ylist, color='white', width=1)
		# 背景色
		ax.set_facecolor('#729ECE')
		# タイトル
		title = '{}  {}'.format(month, total_time)
		ax.set_title(title)

		# X軸の設定 (目盛りを１日毎,範囲は 月初～月末とする)
		ax.xaxis.set_major_locator(mdates.DayLocator())
		ax.xaxis.set_major_formatter(mdates.DateFormatter("%d"))
		# 下限・上限
		ax.set_xlim(xlist[0], xlist[-1])

		# Y軸の設定 (目盛りを１時間毎,範囲は 0:00～16:00とする)
		ax.yaxis.set_major_locator(mdates.HourLocator())
		ax.yaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
		# 下限・上限
		ax.set_ylim(dt.strptime('00:00','%H:%M'), dt.strptime('16:00','%H:%M'))

	# 円グラフ
	pie_axes = [
		pie_ax1, pie_ax2, pie_ax3, pie_ax4, pie_ax5, pie_ax6, pie_ax7, pie_ax8, pie_ax9, pie_ax10, pie_ax11, pie_ax12
	]

	for (data, ax) in zip(pie_data, pie_axes):
		if data[0] == 0 and data[1] == 0:
			classification = '', '', '勤怠記録なし'
		else:
			classification = '出退勤記録', '出勤記録のみ', '勤怠記録なし'

		ax.pie(data, labels=classification, autopct='%.0f%%', textprops={'size': 'large'},)


	# グラフ間を自動調整
	plt.tight_layout()
	# グラフプロット
	graph = Output_Graph()

	return graph
