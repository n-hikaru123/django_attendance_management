from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import calendar
import sys
from django.contrib.auth.models import User
sys.path.append('../../')
from attendance.models import Attendances
from . import graph

class GraphInfoByUserIdService(LoginRequiredMixin, TemplateView):

    def get_graphinfo_by_userid(self, user_id):
        # ユーザー名を取得
        user = User.objects.values('username').filter(id=user_id)
        user_name = user[0]['username']

        # 対象ユーザーの勤怠記録があるか確認
        is_attendance = Attendances.objects.filter(user_id=user_id).exists()

        # 円グラフ
        pie_data = self.pie_chart(user_id, is_attendance)

        # 棒グラフ
        bar_data = self.bar_graph(user_id, is_attendance)
        arr_x_all_days = bar_data[0]
        arr_y_times = bar_data[1]
        arr_month = bar_data[2]
        arr_total_time = bar_data[3]
        total_time = bar_data[4]

        # 文字列で渡す
        chart = graph.Plot_Graph(arr_x_all_days, arr_y_times, arr_month, arr_total_time, pie_data)
        return user_name, total_time, chart


    # 棒グラフ
    def bar_graph(self, user_id, is_attendance):

        # 対象期間のそれぞれの対象月に属するすべての日
        arr_x_all_days = []
        # 対象期間のそれぞれの対象月に属するすべての日のそれぞれの労働時間
        arr_y_times = []
        # 対象期間のそれぞれの対象月
        arr_month = []
        # 対象期間のそれぞれの対象月の労働時間の合計
        arr_total_time = []
        # 対象期間すべての労働時間の合計
        total_time_all = timedelta()

        # 対象ユーザーの勤怠記録がある場合
        if is_attendance:
            attendances = Attendances.objects.filter(
                user_id=user_id,
            )

            for i in range(12):

                target = datetime.today() - relativedelta(months=i)

                # 対象の月の属する日をすべて取得
                x_all_days = self.get_all_days(target)

                month = target.strftime('%Y-%m')

                # 取得した対象ユーザーの勤怠記録のうち、対象月の勤怠記録を取得
                attendance_leave_times = self.get_attendances_date_by_months(attendances, month)

                # 対象月の勤怠記録の取得データに登録されていない日の情報を追加
                not_time = {'attendance_time': None, 'leave_time': None}
                for day in x_all_days:
                    if not day in attendance_leave_times:
                        none_data = {
                            day: not_time
                        }
                        attendance_leave_times.update(none_data)

                attendance_leave_times = sorted(attendance_leave_times.items())

                # 対象の１月間の勤務時間のリストを作成
                y_times = []
                total_time_month = timedelta()
                for data in attendance_leave_times:
                    attendance_time = data[1]['attendance_time']
                    leave_time = data[1]['leave_time']
                    if not attendance_time is None and not leave_time is None:
                        # datetime型での計算
                        time_timedelta = leave_time - attendance_time
                        total_time_month += time_timedelta
                        str_time = str(time_timedelta)
                        y_times.append(str_time)
                    else:
                        y_times.append('00:00:00')
                total_time_all += total_time_month
                # datetime.timedeltaを文字列「◯時間◯分」に変換する処理
                str_total_time = self.change_display_time(total_time_month)
                arr_x_all_days.append(x_all_days)
                arr_y_times.append(y_times)
                arr_month.append(month)
                arr_total_time.append(str_total_time)

            # datetime.timedeltaを文字列「◯時間◯分」に変換する処理
            total_time_all = self.change_display_time(total_time_all)

            return arr_x_all_days, arr_y_times, arr_month, arr_total_time, total_time_all


        # 対象ユーザーの勤怠記録がない場合、空グラフを表示
        else:
            for i in range(12):
                target = datetime.today() - relativedelta(months=i)
                # 対象の月の属する全日を取得
                x_all_days = self.get_all_days(target)

                y_times = ['00:00:00' for _ in range(len(x_all_days))]
                month = target.strftime('%Y-%m')

                str_total_time = self.change_display_time(timedelta())

                arr_x_all_days.append(x_all_days)
                arr_y_times.append(y_times)
                arr_month.append(month)
                arr_total_time.append(str_total_time)

            total_time_all = self.change_display_time(timedelta())

            return arr_x_all_days, arr_y_times, arr_month, arr_total_time, total_time_all


    # 円グラフ
    def pie_chart(self, user_id, is_attendance):

        pie_data = []
        # 対象ユーザーの勤怠記録がある場合
        if is_attendance:
            attendances = Attendances.objects.filter(
                user_id=user_id,
            )

            for i in range(12):
                target = datetime.today() - relativedelta(months=i)
                month = target.strftime('%Y-%m')

                all_days = self.get_all_days(target)
                attendance_leave_times = self.get_attendances_date_by_months(attendances, month)
                # ['出退勤記録両方あり件数', '出勤記録のみ件数', '記録なし件数']
                count_attendance_leave = []
                at_le = 0
                at = 0
                none = 0

                # 対象ユーザーの対象月の勤怠記録がある場合
                if attendance_leave_times:
                    for data in attendance_leave_times.values():
                        attendance_time = data['attendance_time']
                        leave_time = data['leave_time']
                        if attendance_time and leave_time:
                            at_le += 1
                        elif attendance_time:
                            at += 1

                    none = len(all_days) - (at_le + at)
                    count_attendance_leave.extend([at_le, at, none])
                    pie_data.append(count_attendance_leave)

                # 対象ユーザーの対象月の勤怠記録がない場合
                else:
                    # count_attendance_leave = [len(all_days)]
                    count_attendance_leave = [at_le, at, len(all_days)]
                    pie_data.append(count_attendance_leave)

        # 対象ユーザーの勤怠記録が１件も登録されていない場合
        else:
            for i in range(12):
                target = datetime.today() - relativedelta(months=i)
                month = target.strftime('%Y-%m')
                all_days = self.get_all_days(target)

                count_attendance_leave = [0, 0, len(all_days)]
                pie_data.append(count_attendance_leave)

        return pie_data


    # 対象の日の属する月の全日を取得
    def get_all_days(self, target):
        # 対象月の月初を取得
        first_day = target.replace(day=1)
        str_first_day = first_day.strftime('%Y-%m-%d')
        first_day = datetime.strptime(str_first_day, '%Y-%m-%d')

        # 対象月の月末を取得
        last_day = target.replace(day=calendar.monthrange(target.year, target.month)[1])
        str_last_day = last_day.strftime('%Y-%m-%d')
        last_day = datetime.strptime(str_last_day, '%Y-%m-%d')

        x_all_days = []
        day = first_day
        while True:
            if day <= last_day:
                str_day = day.strftime('%Y-%m-%d')
                day = day + timedelta(days=1)
                x_all_days.append(str_day)
            else:
                break
        return x_all_days


    # 取得した対象ユーザーの勤怠記録のうち、対象月の勤怠記録を取得
    def get_attendances_date_by_months(self, attendances, month):
        attendance_leave_times = {}
        for attendance in attendances:
            attendance_time = attendance.attendance_time
            leave_time = attendance.leave_time
            attendance_month = attendance_time.strftime('%Y-%m')
            if attendance_month.startswith(month):
                day_data = {}
                data_key = attendance_time.strftime('%Y-%m-%d')
                day_data['attendance_time'] = attendance_time
                if not leave_time is None:
                    day_data['leave_time'] = leave_time
                else:
                    day_data['leave_time'] = None
                data = {
                    data_key: day_data
                }
                attendance_leave_times.update(data)

        return attendance_leave_times


    # datetime.timedeltaを文字列「◯時間◯分」に変換する処理
    def change_display_time(self, total_time_month):
        sec = total_time_month.total_seconds()
        hours = int(sec//3600)
        minutes = int(sec%3600//60)
        str_total_time = '{}時間{}分'.format(hours, minutes)
        return str_total_time


