from django.apps import AppConfig


class GraphAttendancesInfoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'graph_Attendances_info'
