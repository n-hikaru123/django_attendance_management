from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from .service.GraphInfoByUserIdService import GraphInfoByUserIdService

class AttendancesGraphView(LoginRequiredMixin, TemplateView):
    # 表示するテンプレートを指定
    template_name = 'graph/attendances_graph.html'
    # ログインがされてなかったらリダイレクトされるURL
    login_url = '/accounts/login/'

    def get(self, request, **kwargs):
        user_id = self.kwargs['user_id']
        # 対象のユーザーIDが存在しない場合、404エラー画面を表示する
        fix_request = get_object_or_404(User, pk=user_id)
        graph_info_service = GraphInfoByUserIdService()
        chart = graph_info_service.get_graphinfo_by_userid(user_id)

        context = {
            'user_name':chart[0],
            'total_time':chart[1],
            'chart':chart[2],
        }
        return self.render_to_response(context)
